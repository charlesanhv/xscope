import http from '@/lib/http'
// import { MessageResType } from '@/schemaValidations/common.schema'
import { DataTwitterUsernameResType, SaveStorageBodyType } from '@/schemaValidations/app.schema'

const appApiRequest = {
  getTwitterByUserName: () => http.get<DataTwitterUsernameResType>('/products'),
  getStorage: () => http.get<any>('/storage/load'),
  getUserInfo: () => http.get<any>('/user-info'),
  saveStorage: (body: SaveStorageBodyType) => http.post<any>('/storage/save', body),
  //   update: (id: number, body: UpdateProductBodyType) => http.put<ProductResType>(`/products/${id}`, body),
  //   uploadImage: (body: FormData) =>
  //     http.post<{
  //       message: string
  //       data: string
  //     }>('/media/upload', body),
  removeStorage: (params: any) => http.get<any>(`/products/${params}`)
}

export default appApiRequest
