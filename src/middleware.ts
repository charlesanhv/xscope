import { NextResponse } from 'next/server'
import type { NextRequest } from 'next/server'
// import { sessionStatus } from './lib/utils'
import { cookies } from 'next/headers'

// This function can be marked `async` if using `await` inside
export async function middleware(request: NextRequest) {
  const accessToken = cookies().get('access_token')
  if (!accessToken) {
    return NextResponse.redirect(new URL('/', request.url))
  }
}

// See "Matching Paths" below to learn more
export const config = {
  matcher: ['/app', '/app/:path*']
}
