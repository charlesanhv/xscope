import z from 'zod'

export const SaveStorageBody = z.object({
  name: z.string().min(1).max(256),
  type: z.string().min(1).max(256),
  share: z.number().min(0).max(1),
  status: z.number(),
  stirage_data: z.any().optional()
})

export type SaveStorageBodyType = z.TypeOf<typeof SaveStorageBody>

export const ProductSchema = z.object({
  id: z.number(),
  name: z.string(),
  price: z.number(),
  description: z.string(),
  image: z.string(),
  createdAt: z.date(),
  updatedAt: z.date()
})

export const ProductRes = z.object({
  data: ProductSchema,
  message: z.string()
})

export type ProductResType = z.TypeOf<typeof ProductRes>

export const DataListRes = z.object({
  result: z.object({})
})

export type DataTwitterUsernameResType = z.TypeOf<typeof DataListRes>

export const UpdateProductBody = SaveStorageBody
export const ProductParams = z.object({
  id: z.coerce.number()
})
export type ProductParamsType = z.TypeOf<typeof ProductParams>
