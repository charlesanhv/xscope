import { Metadata } from 'next'
// import FeatureCount from './_components/feature-count'

export const metadata: Metadata = {
  title: 'Features',
  description: 'Features Page'
}

export default async function UIFeature() {
  return (
    <div>
      <div className='h-screen flex flex-col items-center justify-center'>
        <h1 className='text-6xl font-bold tracking-wide text-white mb-12'>Features</h1>
        <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-12'>
          <div className='bg-gray-800 rounded p-8 shadow'>
            <h2 className='text-2xl font-bold text-white mb-4'>Feature 1</h2>
            <p className='text-gray-400 text-lg'>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Qua, aspernatur.
            </p>
          </div>
          <div className='bg-gray-800 rounded p-8 shadow'>
            <h2 className='text-2xl font-bold text-white mb-4'>Feature 2</h2>
            <p className='text-gray-400 text-lg'>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Qua, aspernatur.
            </p>
          </div>
          <div className='bg-gray-800 rounded p-8 shadow'>
            <h2 className='text-2xl font-bold text-white mb-4'>Feature 3</h2>
            <p className='text-gray-400 text-lg'>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Qua, aspernatur.
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}
