import type { Metadata } from 'next'
import type { Viewport } from 'next'
import './globals.css'
import { baseOpenGraph } from './shared-metadata'
import { Toaster } from '@/components/ui/toaster'
import { ThemeProvider } from '@/components/theme-provider'
import AppProvider from './app-provider'
import Header from '@/components/header'
import React from 'react'
import Footer from '@/components/footer'
import { unbounded } from './font'

export const metadata: Metadata = {
  metadataBase: new URL('https://xscope.xyz'),
  title: {
    template: '%s | XScope',
    default: 'XScope'
  },
  description: 'XScope API documentation for Statistr AI',
  icons: {
    icon: {
      url: "/images/App/icon_token.svg",
      type: "image/svg",
    },
    shortcut: { url: "/images/App/icon_token.svg", type: "image/svg" },
  },
  openGraph: baseOpenGraph
}

export const viewport: Viewport = {
  width: '',
  initialScale: 0,
  maximumScale: 0,
  userScalable: false,
  colorScheme: 'dark',
  themeColor: 'black'
  // Also supported by less commonly used
  // interactiveWidget: 'resizes-visual',
}

export default function RootLayout({
  children
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
    <html lang='en' suppressHydrationWarning>
      <body className={unbounded.className}>
        {' '}
        <Toaster />
        <ThemeProvider attribute='class' defaultTheme='system' enableSystem disableTransitionOnChange>
          <AppProvider>
            <Header />
            {children}
            <Footer />
          </AppProvider>
        </ThemeProvider>
      </body>
    </html>
  )
}
