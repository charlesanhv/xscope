'use client'
export default function LoadingPage() {
  return (
    <div className='flex h-screen w-screen items-center justify-center bg-gray-100'>
      <div className='flex flex-col items-center justify-center'>
        <div className='animate-spin rounded-full h-5 w-5 border-b-2 border-blue-500'></div>
        <h1 className='text-6xl font-bold text-gray-800'>
          <span className='animate-[loading-icon 1.5s ease-in infinite]'></span>
        </h1>
      </div>
    </div>
  )
}
