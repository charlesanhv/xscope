import { Metadata } from 'next'
import HomeHeader from '@/components/home/HomeHeader'
import HomePlan from '@/components/home/HomePlan'
export const metadata: Metadata = {
  title: 'XScope Statistr AI',
  description: 'XScope Statistr AI description'
}

export default async function Home() {
  return (
    <main className='flex flex-col items-center pt-16' style={{ position: 'relative' }}>
      <HomeHeader />
      <HomePlan />
    </main>
  )
}
