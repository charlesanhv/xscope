import { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'About',
  description: 'About Page'
}

export default async function UIAbout() {
  return (
    <div className='min-h-screen flex flex-col items-center justify-center bg-slate-50 p-12'>
      <header className='flex flex-col items-center justify-center'>
        <h1 className='text-6xl font-bold tracking-wide text-gray-900'>About XScope</h1>
        <p className='mt-6 text-3xl font-semibold text-gray-700'>
          XScope is a AI-powered tool for social media management.
        </p>
      </header>
      <main className='mx-auto mt-12 w-full max-w-md p-6 bg-white rounded-lg shadow'>
        <section className='space-y-6'>
          <h2 className='text-3xl font-semibold text-gray-900'>What is XScope?</h2>
          <p className='text-xl text-gray-600'>
            XScope is a powerful AI-powered tool for social media management. It allows you to manage multiple social
            media accounts from one place and track your performance in real-time.
          </p>
          <h2 className='text-3xl font-semibold text-gray-900'>Features</h2>
          <ul className='list-disc pl-6'>
            <li className='text-xl text-gray-600'>AI-powered content suggestions</li>
            <li className='text-xl text-gray-600'>Real-time analytics and tracking</li>
            <li className='text-xl text-gray-600'>Multi-account support</li>
          </ul>
        </section>
      </main>
    </div>
  )
}
