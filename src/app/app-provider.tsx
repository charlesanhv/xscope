'use client'
// import { isClient } from '@/lib/http'
import { AccountResType } from '@/schemaValidations/account.schema'
import React, { createContext, useCallback, useContext, useEffect, useState } from 'react'
import Cookies from 'js-cookie'
import appApiRequest from '@/apiRequests/app'

type User = AccountResType['data']

const AppContext = createContext<{
  user: User | null
  // eslint-disable-next-line no-unused-vars
  setUser: (user: User | null) => void
  isAuthenticated: boolean
}>({
  user: null,
  setUser: () => {},
  isAuthenticated: false
})
export const useAppContext = () => {
  const context = useContext(AppContext)
  return context
}
export default function AppProvider({ children }: { children: React.ReactNode }) {
  const sessionToken = Cookies.get('access_token')
  const [user, setUserState] = useState<User | null>(() => {
    // if (isClient()) {
    //   const _user = localStorage.getItem('user')
    //   return _user ? JSON.parse(_user) : null
    // }
    return null
  })
  const isAuthenticated = Boolean(sessionToken)
  const setUser = useCallback(
    (user: User | null) => {
      setUserState(user)
      localStorage.setItem('user', JSON.stringify(user))
    },
    [setUserState]
  )

  useEffect(() => {
    isAuthenticated && appApiRequest.getUserInfo().then((res) => {
      console.log('res', res)
    })
    // const _user = localStorage.getItem('user')
    // setUserState(_user ? JSON.parse(_user) : null)
  }, [isAuthenticated])

  return (
    <AppContext.Provider
      value={{
        user,
        setUser,
        isAuthenticated
      }}
    >
      {children}
    </AppContext.Provider>
  )
}
