import Link from 'next/link'

export default function NotFound() {
  return (
    <div className='h-screen flex flex-col items-center justify-center text-center py-20'>
      <h2 className='text-6xl font-bold tracking-wide text-primary'>404</h2>
      <p className='text-2xl font-light text-white'>Could not find requested resource</p>
      <Link
        href='/'
        className='mt-10 px-10 py-4 bg-primary rounded-lg text-white text-xl font-semibold hover:bg-primary-dark'
      >
        Return Home
      </Link>
    </div>
  )
}
