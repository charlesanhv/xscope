import React from "react";
// import Link from "next/link";
import ComponentLayout from '@/components/recap/ComponentLayout';
import { LIST_DATA_TWITTER } from '@/constants';
export default async function UIRecapItem({ params, searchParams }: any) {
    const timeRange = searchParams?.time ?? 3

    const itemSelected = LIST_DATA_TWITTER?.find((item) => item?.slug === params.slug)

    return (
        <main className="h-screen pt-[136px] px-[72px]" style={{ background: 'linear-gradient(111deg, #46454E 0%, #1B1B1C 33.33%, #161616 50.5%, #211E33 100%)' }}>
            <ComponentLayout itemSelected={itemSelected} timeRange={timeRange}/>
        </main >
    );
}
