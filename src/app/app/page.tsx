'use client'
// import { Metadata } from "next";
import { useCallback, useEffect, useState } from 'react'
import Image from 'next/image'
import IconSearch from '../../../public/images/App/icon_search.svg'
import IconTwitter from '../../../public/images/App/icon_Twittericon_Twitter.svg'
import IconList from '../../../public/images/App/icon_List.svg'
import BgDefaultButtonItem from '../../../public/images/App/bg_default_button_item.svg'
import BgHoverButtonItem from '../../../public/images/App/bg_hover_button_item.svg'
import IconArrow from '../../../public/images/App/icon_arrow.svg'
import IconArrowShow from '../../../public/images/App/icon_arrow_show.svg'
import '../../components/style/App.css'
import { LIST_DATA_TWITTER } from '@/constants'
import { Dialog, DialogTrigger } from '@/components/ui/dialog'
import { archivo } from '../font'
import Logo from '../../../public/images/App/logo_twitter_dummy.svg'
import IconAdd from '../../../public/images/App/icon_add.svg'
import InputSearch from '@/components/common/InputSearch'
import IconFire from '@/components/common/IconFire'
import { motion } from 'framer-motion'
import ModalListProfile from '@/components/app/ModalListProfile'
import { SkeletonDemo } from '@/components/skeleton'
// import { sessionStatus } from '@/lib/utils'
// import { redirect } from 'next/navigation'
export default function UIApp() {
  const [valueSearch, setValueSearch] = useState('')
  const [dataTwitter, setDataTwitter] = useState<any[]>([])
  const [loading, setLoading] = useState<Boolean>(false)
  const [showSuggest, setShowSuggest] = useState<Boolean>(true)
  const [addItemToFollow, setAddItemToFollow] = useState<Boolean>(false)
  const [listProfile, setListProfile] = useState<any[]>([])
  const [isProfileUpdated, setIsProfileUpdated] = useState(0)

  // motion

  const spring = {
    // type: 'spring',
    // damping: 10,
    // stiffness: 100
    ease: 'easeInOut',
    duration: 0.5
  }

  const handleAddListProfile = (item: any) => {
    if (!listProfile.some((profile) => profile === item)) {
      setListProfile([...listProfile, item])
      setDataTwitter([])
    }
  }

  const handeleGetDataModal = (listData: any[]) => {
    setAddItemToFollow(true)
    const existingIds = new Set(listProfile.map((item: any) => item.id))
    const newData = listData.filter((item: any) => !existingIds.has(item.id))
    setListProfile([...listProfile, ...newData])
  }

  const handleChagelistProfile = (listData: any[]) => {
    setListProfile(listProfile.filter((item) => !listData.includes(item)))
  }

  const handlePaste = (event: any) => {
    if (dataTwitter && dataTwitter.length > 0 && event.clipboardData.getData('Text') !== valueSearch) {
      setDataTwitter([])
    }
    setValueSearch(event.clipboardData.getData('Text'))
  }

  const handleKeyDown = (event: any) => {
    // Ngăn chặn mọi thao tác từ bàn phím ngoại trừ các phím điều khiển như Tab hoặc các phím chức năng
    if (!event.ctrlKey && !event.metaKey) {
      event.preventDefault()
    }
  }

  //   const handleChange = (event: any) => {
  //     event.preventDefault()
  //   }
  //   useLayoutEffect(() => {
  //     const session = sessionStatus
  //     if (!session) {
  //       redirect('/')
  //     }
  //   }, [])

  const getDataTwitterByUsername = useCallback(async () => {
    setLoading(true)
    const options = {
      method: 'GET',
      headers: {
        'X-RapidAPI-Key': 'ad7ebba2fdmsh5ecd11f660c20f7p1fb61fjsn58f3b298a9a0',
        'X-RapidAPI-Host': 'twitter241.p.rapidapi.com'
      }
    }
    try {
      const response = await fetch(`https://twitter241.p.rapidapi.com/user?username=${valueSearch}`, options)
      // Assuming the response is JSON. Use response.text() if it's text.
      const result = await response.json()
      const tmpdataTwitter = result?.result?.data?.user?.result?.legacy
      if (tmpdataTwitter) {
        const arrayDataTwitter = [tmpdataTwitter]
        setDataTwitter(arrayDataTwitter)
      } else {
        setDataTwitter([])
        console.error('Twitter data not found:', result)
      }
      setLoading(false)
    } catch (error) {
      console.error(error)
      setLoading(false)
    }
  }, [valueSearch]) // Adding valueSearch as a dependency

  useEffect(() => {
    if (valueSearch.length > 0) {
      getDataTwitterByUsername()
    }
  }, [getDataTwitterByUsername, valueSearch.length])

  useEffect(() => {
    if (listProfile?.length === 0 || addItemToFollow) return
    setIsProfileUpdated(1)
    const timer = setTimeout(() => {
      setIsProfileUpdated(2)
    }, 1000)
    const timer2 = setTimeout(() => {
      setIsProfileUpdated(0)
    }, 1200)

    return () => {
      clearTimeout(timer)
      clearTimeout(timer2)
    }
  }, [addItemToFollow, listProfile])

  useEffect(() => {
    if (!addItemToFollow) return
    const timeout = setTimeout(() => {
      setAddItemToFollow(false)
    }, 1000)
    return () => clearTimeout(timeout)
  }, [addItemToFollow])

  //   const listDataTwitter = useMemo(
  //     () => (dataTwitter && dataTwitter.length > 0 ? dataTwitter : LIST_DATA_TWITTER),
  //     [dataTwitter]
  //   )
  return (
    <div
      className='flex h-screen py-2'
      style={{ background: 'linear-gradient(111deg, #46454E 0%, #1B1B1C 33.33%, #161616 50.5%, #211E33 100%)' }}
    >
      <div className='w-full max-w-[1024px] mx-auto pt-[214px] flex flex-col gap-[40px] justify-start items-center'>
        <span
          className='bg-clip-text text-[32px] font-[300] tracking-[-0.32px]'
          style={{
            background:
              'linear-gradient(90deg, rgba(239, 239, 239, 0.90) 0%, rgba(254, 254, 254, 0.90) 49.5%, rgba(239, 239, 239, 0.88) 100%)',
            WebkitBackgroundClip: 'text',
            WebkitTextFillColor: 'transparent'
          }}
        >
          Start here
        </span>
        <InputSearch
          valueSearch={valueSearch}
          onPaste={handlePaste}
          onKeyDown={handleKeyDown}
          changeLink={false}
          link={''}
          placeholder={'Search by user name ex @rolex'}
        />
        {valueSearch ? (
          <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} transition={spring}>
            <div className='flex flex-col pl-[80px] w-[512px]'>
              {loading ? <SkeletonDemo /> : null}
              {dataTwitter &&
                dataTwitter.length > 0 &&
                dataTwitter?.map((item: any, index: any) => {
                  return (
                    <div
                      key={index}
                      className='flex gap-4 items-start p-3 cursor-pointer w-full item_list_twit'
                      onClick={() => {
                        handleAddListProfile(item)
                        setValueSearch('')
                      }}
                    >
                      <Image
                        src={item?.profile_image_url_https}
                        alt={item?.screen_name}
                        width={32}
                        height={32}
                        className='rounded-[32px]'
                      />
                      <div className='flex flex-col'>
                        <span className='text-[16px] font-[300] tracking-[-0.16px] text-[#fafafae6]'>{item?.name}</span>
                        <span className='text-[16px] font-[300] tracking-[0.32px] text-[#fafafa99]'>
                          @{item?.screen_name}
                        </span>
                      </div>
                    </div>
                  )
                })}
              {dataTwitter && dataTwitter.length === 0 && !loading && (
                <span className='text-[16px] font-[300] tracking-[-0.16px] text-[#fafafae6]'>No results found</span>
              )}
            </div>
          </motion.div>
        ) : (
          <>
            <div className='w-full flex justify-center gap-3'>
              <Dialog>
                <DialogTrigger asChild>
                  <div className='flex items-start gap-3 w-[342px] h-[88px] py-[20px] px-6 rounded-[6px] cursor-pointer relative button_item'>
                    <Image
                      src={IconTwitter}
                      alt='IconTwitter'
                      width={32}
                      height={32}
                      className='pt-1'
                      style={{ zIndex: 50 }}
                    />
                    <div className='flex flex-col' style={{ zIndex: 50 }}>
                      <span className='text-[14px] font-[400] tracking-[-0.14px] text-[#C5D8EE]'>
                        Add from your Following
                      </span>
                      <div className='flex gap-[6px] items-end'>
                        <span className={`${archivo.className} text-[20px] font-[400] tracking-[0.2px] text-[#F5F5F5]`}>
                          1,429
                        </span>
                        <span
                          className={`${archivo.className} text-[16px] font-[300] tracking-[0.32px] text-[#F5F5F599]`}
                        >
                          following
                        </span>
                      </div>
                    </div>
                    <Image src={BgDefaultButtonItem} alt={'BgDefaultButtonItem'} className='BgDefaultButtonItem' />
                    <Image src={BgHoverButtonItem} alt={'BgHoverButtonItem'} className='BgHoverButtonItem' />
                  </div>
                </DialogTrigger>
                <ModalListProfile type={'following'} onGetDataModal={handeleGetDataModal} />
              </Dialog>
              <Dialog>
                <DialogTrigger asChild>
                  <div className='flex items-start gap-3 w-[342px] h-[88px] py-[20px] px-6 rounded-[6px] cursor-pointer relative button_item'>
                    <Image
                      src={IconList}
                      alt='IconList'
                      width={32}
                      height={32}
                      className='pt-1'
                      style={{ zIndex: 50 }}
                    />
                    <div className='flex flex-col flex-1' style={{ zIndex: 50 }}>
                      <span className='text-[14px] font-[400] tracking-[-0.14px] text-[#C5D8EE] text-start'>
                        Your Profile
                      </span>
                      <div className='flex gap-[6px] items-end'>
                        <span className={`${archivo.className} text-[20px] font-[400] tracking-[0.2px] text-[#F5F5F5]`}>
                          {listProfile?.length}
                        </span>
                        <span
                          className={`${archivo.className} text-[16px] font-[300] tracking-[0.32px] text-[#F5F5F599]`}
                        >
                          profile
                        </span>
                      </div>
                    </div>
                    <div
                      className={`flex items-center my-auto pr-2 ${isProfileUpdated === 0 ? 'profile-change-down' : isProfileUpdated === 2 ? 'profile-change-up' : 'profile-change'}`}
                      style={{ zIndex: 50 }}
                    >
                      <Image src={IconArrow} alt={'IconArrow'} />
                      <span className='text-[16px] font-[500] tracking-[-0.16px] text-[#C5D8EE]'>1</span>
                    </div>
                    <Image src={BgDefaultButtonItem} alt={'BgDefaultButtonItem'} className='BgDefaultButtonItem' />
                    <Image src={BgHoverButtonItem} alt={'BgHoverButtonItem'} className='BgHoverButtonItem' />
                  </div>
                </DialogTrigger>
                <ModalListProfile
                  type={'your_profile'}
                  listProfile={listProfile}
                  onEditListProfile={handleChagelistProfile}
                />
              </Dialog>
            </div>
            <div className='pt-6 flex flex-col gap-6'>
              <div className='flex items-center gap-4'>
                <div className='w-[454px] h-[1px] bg-[#998dc61f]' />
                <span className='text-[16px] font-[400] tracking-[-0.16px] text-[#f5f5f599]'>Suggest</span>
                <div className='w-[204px] h-[1px] bg-[#998dc61f]' />
                <div
                  className='flex items-center gap-[2px] h-[24px] cursor-pointer justify-center'
                  onClick={() => setShowSuggest(!showSuggest)}
                >
                  <Image
                    src={IconArrowShow}
                    alt={'IconArrowShow'}
                    style={{
                      transform: showSuggest ? 'rotate(180deg)' : 'rotate(0deg)',
                      transition: 'transform 0.3s ease-in-out'
                    }}
                  />
                  <span className={`${archivo.className} text-[13px] font-[400] tracking-[0.26px] text-[#f5f5f599]`}>
                    {showSuggest ? 'Hide' : 'Show'} All
                  </span>
                </div>
                <div className='flex-1 h-[1px] bg-[#998dc61f]' />
              </div>
              <div
                className='flex flex-wrap gap-2 justify-center w-full mx-auto'
                style={{ opacity: showSuggest ? 1 : 0, transition: 'opacity 0.3s ease-in-out' }}
              >
                {LIST_DATA_TWITTER?.map((item, index) => {
                  return (
                    <div
                      key={index}
                      className='flex items-center gap-[8px] h-[40px] w-max px-4 border border-[#998dc652] rounded-[20px] cursor-pointer suggest_item'
                      style={{ paddingRight: item?.hot ? '8px' : '16px', zIndex: 200 }}
                      onClick={() => handleAddListProfile(item)}
                    >
                      <Image src={IconSearch} alt='IconSearch' className='iconSearch' width={16} height={16} />
                      <Image src={IconAdd} alt='IconSearch' className='iconAdd' width={16} height={16} />
                      <Image src={Logo} width={22} height={22} alt='logo' className='rounded-[22px] ' />
                      <span className={`${archivo.className} text-[14px] font-[400] tracking-[0.28px] text-[#F5F5F5]`}>
                        {item?.name}
                      </span>
                      {item?.hot ? <IconFire /> : null}
                    </div>
                  )
                })}
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  )
}
