/* eslint-disable @next/next/no-img-element */
import { ImageResponse } from 'next/og'

// Image metadata
export const alt = 'About XScope'
export const size = {
  width: 1200,
  height: 630
}

export const contentType = 'image/png'

// Image generation
export default async function Image() {
  const imageUrl = 'https://xscope.xyz/images/Home/Group_5596.png'
  return new ImageResponse(
    (
      // ImageResponse JSX element
      <div
        style={{
          fontSize: 128,
          background: 'white',
          width: '100%',
          height: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        //   backgroundImage: `url(${imageUrl})`,
        //   backgroundRepeat: 'no-repeat',
        //   backgroundSize: 'cover',
        //   backgroundClip: 'content-box'
        }}
      >
        {/* About XScope */}
        <img src={imageUrl} alt='About XScope' />
      </div>
    ),
    // ImageResponse options
    {
      // For convenience, we can re-use the exported opengraph-image
      // size config to also set the ImageResponse's width and height.
      ...size
    }
  )
}
