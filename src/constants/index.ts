import Logo from '../../public/images/App/logo_twitter_dummy.svg'
import logoEth from '../../public/images/App/logo_eth.svg'
import logoBtc from '../../public/images/App/logo_btc.svg'
import logoSol from '../../public/images/App/logo_sol.svg'
import iconBusd from '../../public/images/App/icon_busd.svg'

export const DUMMY_CONTENT_ITEM = [
  '📢 3 days Recap with @Statistr 📢',
  `1.Ethereum's London Upgrade: Vitalik dives deep into the impact of EIP-1559 and the progress towards Ethereum 2.0. `,
  `2.NFT Boom Continues: Insights on the latest developments in the NFT space and Ethereum's role in shaping its future.`,
  `3.DeFi Evolution: Vitalik discusses the evolving landscape of decentralized finance, highlighting challenges and opportunities.`,
  `4.Layer 2 Solutions: Updates on Ethereum's scaling efforts with layer 2 solutions and the quest for greater throughput.`,
  `5.Community AMA: Recap of Vitalik's community Ask Me Anything session, covering a wide range of topics from governance to technical advancements.`,
  `6.Sustainability Initiatives: Ethereum's journey towards environmental sustainability and plans to reduce its carbon footprint.`,
  `7.Future of Ethereum: Insights into Vitalik's vision for Ethereum's future and its role in shaping the decentralized web.`,
  `8.Stay tuned for more updates from the forefront of blockchain innovation! #Ethereum #Crypto #DeFi`
]

export const LIST_DATA_TWITTER = [
  {
    id: 1,
    name: 'Statistr | Hiring Data Contributor',
    user_name: 'statistr_',
    slug: 'statistr-hiring-data-contributor',
    logo: Logo,
    content: DUMMY_CONTENT_ITEM
  },
  {
    id: 2,
    name: 'Statistrek_ Megamall',
    user_name: 'statistrek.m',
    slug: 'statistrek-megamall',
    logo: Logo,
    hot: true,
    content: DUMMY_CONTENT_ITEM
  },
  {
    id: 3,
    name: 'Statist Cookies',
    user_name: 'statistr_s.cookies',
    slug: 'statistr-cookies',
    hot: true,
    logo: Logo,
    content: DUMMY_CONTENT_ITEM
  },
  {
    id: 4,
    name: 'Yung Patek',
    user_name: 'louis.vertical',
    slug: 'louis-vertical',
    logo: Logo,
    content: DUMMY_CONTENT_ITEM
  },
  {
    id: 5,
    name: 'Travis Nguyen',
    user_name: 'travis.sconguyen',
    slug: 'travis-sconguyen',
    logo: Logo,
    content: DUMMY_CONTENT_ITEM
  },
  {
    id: 6,
    name: 'PoP Btc Magazine',
    user_name: 'the_beck',
    slug: 'the-beck',
    logo: Logo,
    content: DUMMY_CONTENT_ITEM
  },
  {
    id: 7,
    name: 'Don Toliver',
    user_name: 'don_toliver',
    slug: 'don-toliver',
    logo: Logo,
    content: DUMMY_CONTENT_ITEM
  },
  {
    id: 8,
    name: 'Travis JBec',
    user_name: 'travis_jbec',
    slug: 'travis-jbec',
    logo: Logo,
    content: DUMMY_CONTENT_ITEM
  },
  {
    id: 9,
    name: 'Mad Meloodic',
    user_name: 'mad_meloodic',
    slug: 'mad-meloodic',
    logo: Logo,
    content: DUMMY_CONTENT_ITEM
  },
  {
    id: 10,
    name: 'Kiuchangyie',
    user_name: 'kiuchangyie',
    slug: 'kiuchangyie',
    logo: Logo,
    content: DUMMY_CONTENT_ITEM
  },
  {
    id: 11,
    name: 'Mike Melodic',
    user_name: 'mike_melodic',
    slug: 'mike-melodic',
    logo: Logo,
    content: DUMMY_CONTENT_ITEM
  },
  {
    id: 10,
    name: 'TrapLife',
    user_name: 'trap_life',
    slug: 'trap-life',
    logo: Logo,
    content: DUMMY_CONTENT_ITEM
  }
]

export const DUMMY_LIST_IMAGE = [Logo, Logo, Logo, Logo, Logo]

export const TYPE_DETECT_ITEM = {
  type_1: 'project',
  type_2: 'people',
  type_3: 'organization'
}
export const LIST_DETECT_ITEM = [
  {
    name: 'Ethereum',
    logo: logoEth,
    symbol: 'ETH',
    type: TYPE_DETECT_ITEM.type_1,
    slug: 'dummy-type-1-1',
    price: '$0.04921',
    change_24: '+0.12%',
    description:
      'Ethereum is a decentralized open-source blockchain system that features its own cryptocurrency, Ether. ETH works as a platform for numerous other cryptocurrencies, as well as for the execution of decentralized smart contracts.',
    sector: ['Tag_1', 'Tag_2', 'Tag_3']
  },
  {
    name: 'Changpeng ZHao',
    logo: '/images/dummy/zhao.svg',
    primary_job_title: 'CEO',
    type: TYPE_DETECT_ITEM.type_2,
    slug: 'dummy-type-1-2',
    description:
      "The Chinese-Canadian coder cut his teeth building high-frequency trading systems for Wall Street's flash boys.",
    sector: ['Tag_1', 'Tag_2', 'Tag_3']
  },
  {
    name: 'Muliticoin Capital',
    logo: '/images/dummy/multicoint.svg',
    type: TYPE_DETECT_ITEM.type_3,
    slug: 'dummy-type-3-1',
    description:
      'Multicoin Capital is an Austin-based venture capital company that offers three types of value for investors',
    sector: ['Tag_1', 'Tag_2', 'Tag_3'],
    number_of_invests: 124,
    total_invest: '$800.52M'
  },
  {
    name: 'Ethereum',
    logo: logoEth,
    symbol: 'ETH',
    type: TYPE_DETECT_ITEM.type_1,
    slug: 'dummy-type-1-1',
    price: '$0.04921',
    change_24: '+0.12%',
    description:
      'Ethereum is a decentralized open-source blockchain system that features its own cryptocurrency, Ether. ETH works as a platform for numerous other cryptocurrencies, as well as for the execution of decentralized smart contracts.',
    sector: ['Tag_1', 'Tag_2', 'Tag_3']
  },
  {
    name: 'Changpeng ZHao',
    logo: '/images/dummy/zhao.svg',
    primary_job_title: 'CEO',
    type: TYPE_DETECT_ITEM.type_2,
    slug: 'dummy-type-1-2',
    description:
      "The Chinese-Canadian coder cut his teeth building high-frequency trading systems for Wall Street's flash boys.",
    sector: ['Tag_1', 'Tag_2', 'Tag_3']
  },
  {
    name: 'Muliticoin Capital',
    logo: '/images/dummy/multicoint.svg',
    type: TYPE_DETECT_ITEM.type_3,
    slug: 'dummy-type-3-1',
    description:
      'Multicoin Capital is an Austin-based venture capital company that offers three types of value for investors',
    sector: ['Tag_1', 'Tag_2', 'Tag_3'],
    number_of_invests: 124,
    total_invest: '$800.52M'
  },
  {
    name: 'Ethereum',
    logo: logoEth,
    symbol: 'ETH',
    type: TYPE_DETECT_ITEM.type_1,
    slug: 'dummy-type-1-1',
    price: '$0.04921',
    change_24: '+0.12%',
    description:
      'Ethereum is a decentralized open-source blockchain system that features its own cryptocurrency, Ether. ETH works as a platform for numerous other cryptocurrencies, as well as for the execution of decentralized smart contracts.',
    sector: ['Tag_1', 'Tag_2', 'Tag_3']
  },
  {
    name: 'Changpeng ZHao',
    logo: '/images/dummy/zhao.svg',
    primary_job_title: 'CEO',
    type: TYPE_DETECT_ITEM.type_2,
    slug: 'dummy-type-1-2',
    description:
      "The Chinese-Canadian coder cut his teeth building high-frequency trading systems for Wall Street's flash boys.",
    sector: ['Tag_1', 'Tag_2', 'Tag_3']
  },
  {
    name: 'Muliticoin Capital',
    logo: '/images/dummy/multicoint.svg',
    type: TYPE_DETECT_ITEM.type_3,
    slug: 'dummy-type-3-1',
    description:
      'Multicoin Capital is an Austin-based venture capital company that offers three types of value for investors',
    sector: ['Tag_1', 'Tag_2', 'Tag_3'],
    number_of_invests: 124,
    total_invest: '$800.52M'
  },
  {
    name: 'Ethereum',
    logo: logoEth,
    symbol: 'ETH',
    type: TYPE_DETECT_ITEM.type_1,
    slug: 'dummy-type-1-1',
    price: '$0.04921',
    change_24: '+0.12%',
    description:
      'Ethereum is a decentralized open-source blockchain system that features its own cryptocurrency, Ether. ETH works as a platform for numerous other cryptocurrencies, as well as for the execution of decentralized smart contracts.',
    sector: ['Tag_1', 'Tag_2', 'Tag_3']
  },
  {
    name: 'Changpeng ZHao',
    logo: '/images/dummy/zhao.svg',
    primary_job_title: 'CEO',
    type: TYPE_DETECT_ITEM.type_2,
    slug: 'dummy-type-1-2',
    description:
      "The Chinese-Canadian coder cut his teeth building high-frequency trading systems for Wall Street's flash boys.",
    sector: ['Tag_1', 'Tag_2', 'Tag_3']
  },
  {
    name: 'Muliticoin Capital',
    logo: '/images/dummy/multicoint.svg',
    type: TYPE_DETECT_ITEM.type_3,
    slug: 'dummy-type-3-1',
    description:
      'Multicoin Capital is an Austin-based venture capital company that offers three types of value for investors',
    sector: ['Tag_1', 'Tag_2', 'Tag_3'],
    number_of_invests: 124,
    total_invest: '$800.52M'
  }
]

export const DUMMY_CONTENT = `
<p>📢 3 days Recap with @Statistrssssss 📢</p>
<p>1.Ethereum's London Upgrade: Vitalik dives deep into the impact of EIP-1559 and the progress towards Ethereum 2.0. </p>
<p>2.NFT Boom Continues: Insights on the latest developments in the NFT space and Ethereum's role in shaping its future.</p>
<p>3.DeFi Evolution: Vitalik discusses the evolving landscape of decentralized finance, highlighting challenges and opportunities.</p>
<p>4.Layer 2 Solutions: Updates on Ethereum's scaling efforts with layer 2 solutions and the quest for greater throughput.</p>
<p>5.Community AMA: Recap of Vitalik's community Ask Me Anything session, covering a wide range of topics from governance to technical advancements.</p>
<p>6.Sustainability Initiatives: Ethereum's journey towards environmental sustainability and plans to reduce its carbon footprint.</p>
<p>7.Future of Ethereum: Insights into Vitalik's vision for Ethereum's future and its role in shaping the decentralized web.</p>
8.Stay tuned for more updates from the forefront of blockchain innovation! #Ethereum #Crypto #DeFi`

export const LIST_COIN = [
  {
    name_exchanges: '',
    list_coin: [
      {
        id: 1,
        logo: logoEth,
        symbol: 'ETH',
        coins: 0.0024
      },
      {
        id: 2,
        logo: logoBtc,
        symbol: 'BTC',
        coins: 0.0024
      },
      {
        id: 3,
        logo: logoSol,
        symbol: 'SOL',
        coins: 0.0024
      }
    ]
  },
  {
    name_exchanges: 'Ethereum/ ERC20',
    list_coin: [
      {
        id: 4,
        logo: logoEth,
        symbol: 'ETH',
        coins: 12.2512
      },
      {
        id: 5,
        logo: logoBtc,
        symbol: 'BTC',
        coins: 12.2512
      }
    ]
  },
  {
    name_exchanges: 'BC Mainnet/ BEP2',
    list_coin: [
      {
        id: 6,
        logo: iconBusd,
        symbol: 'BUSD',
        coins: 12.2512
      }
    ]
  },
  {
    name_exchanges: 'BC Mainnet/ BEP2',
    list_coin: [
      {
        id: 7,
        logo: iconBusd,
        symbol: 'BUSD',
        coins: 12.2512
      }
    ]
  }
]
