'use client'

import '../style/App.css'
import React, { useState } from 'react'
// import Link from "next/link";
import History from '@/components/app/History'
import ContentItem from '@/components/app/ContentItem'
import InputSearch from '@/components/common/InputSearch'
import Detected from '@/components/app/Detected'
export default function ComponentLayout({ itemSelected, timeRange }: any) {
  const [getReport, setGetReport] = useState<Boolean>(false)

  return (
    <div className='w-full max-w-[1222px] mx-auto flex flex-col gap-10'>
      <div className='flex items-center justify-center'>
        <InputSearch valueSearch={''} changeLink={true} link={'/app'} placeholder={'Search'} onPaste={undefined} onKeyDown={undefined} />
      </div>
      <div className='flex items-start gap-10'>
        <History itemActive={itemSelected} />
        <ContentItem
          setGetReport={setGetReport}
          timeRange={timeRange}
          itemActive={itemSelected}
          getReport={getReport}
        />
        <Detected getReport={getReport} />
      </div>
    </div>
  )
}
