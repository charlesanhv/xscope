'use client'

import { sessionStatus } from '@/lib/utils'
import { useEffect } from 'react'
import { redirect } from 'next/navigation'

export default function withAuth(Compoennt: any) {
  return function WithAuth(props: any) {
    const session = sessionStatus
    useEffect(() => {
      if (!session) {
        return redirect('/')
      }
    }, [session])
    if (!sessionStatus) {
      return null
    }
    return <Compoennt {...props} />
  }
}
