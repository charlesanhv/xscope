'use client'
import React, { useState } from 'react'
import { archivo } from '@/app/font'
import Billing from '../../../public/images/App/Billing.png'
import IconCoin from '../../../public/images/App/icon_coin.svg'
import IconAdd from '../../../public/images/App/icon_add_2.svg'
import '../style/PopupPayment.css'
import Image from 'next/image'
import { motion } from 'framer-motion'
import { LIST_COIN } from '@/constants'
import { Button } from '../ui/button'
export default function LayoutPayCoin({ onChangeQR }: any) {
  const [itemSelected, setItemSelect] = useState<any>()
  const spring = {
    // type: 'spring',
    // damping: 10,
    // stiffness: 100
    ease: 'easeInOut',
    duration: 0.5
  }
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={spring}
      className='flex gap-6 z-[300] mx-[-24px]'
    >
      <div className='px-6 flex flex-col gap-6 w-[446px]'>
        <span className='text-[#F5F5F5] text-[24px] font-[500]'>Payment Details</span>
        <div className='pt-4 flex flex-col gap-6'>
          <span className='text-[#F5F5F5] text-[16px] font-[500]'>Choose coin</span>
          <div className='flex flex-col gap-6 max-h-[284px] listCoins overflow-y-scroll'>
            {LIST_COIN?.map((item, index) => (
              <div key={index} className='flex flex-col gap-3'>
                {item?.name_exchanges ? (
                  <span className='text-[#C5D8EE] text-[14px] font-[500]'>{item?.name_exchanges}</span>
                ) : null}
                <div className='flex items-center flex-wrap gap-3'>
                  {item?.list_coin?.map((coin, index) => (
                    <div
                      className='w-[190px] cursor-pointer flex items-center justify-between gap-3 p-3 border border-[#998dc63d] rounded-[8px] hover:border-[#998dc652] hover:bg-[#c5d8ee1f]'
                      style={{
                        transition: 'all 0.3s ease',
                        borderColor: itemSelected === coin?.id ? '#8A66D8' : '#998dc63d'
                      }}
                      key={index}
                      onClick={() => setItemSelect(coin?.id)}
                    >
                      <Image src={coin?.logo} alt='logo' width={24} height={24} />
                      <div className='flex items-center gap-[6px]'>
                        <span className={`${archivo.className} text-[#F5F5F5] text-[16px] font-[400]`}>
                          {coin?.coins}
                        </span>
                        <span className={`${archivo.className} text-[#f5f5f599] text-[16px] font-[400] uppercase`}>
                          {coin?.symbol}
                        </span>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className='w-[330px] h-full px-6 pt-6 pb-12 relative'>
        <Image src={Billing} alt='Billing' className='w-full absolute top-0 left-0 right-0 bottom-0 h-full z-[-1]' />
        <div className='z-[200] flex flex-col gap-14'>
          <div className='flex flex-col gap-4'>
            <span className='text-[#f5f5f599] text-[14px] font-[500] tracking-[-0.14px]'>Order Summary</span>
            <div className='flex gap-3 py-3'>
              <Image src={IconCoin} alt='IconCoin' width={32} height={32} />
              <div className='flex flex-col'>
                <span className={`${archivo.className} text-[#F5F5F5] text-[18px] font-[500] tracking-[-0.18px]`}>
                  10 Credits
                </span>
                <span className={`${archivo.className} text-[#f5f5f599] text-[16px] font-[300] tracking-[-0.16px]`}>
                  $20.5
                </span>
              </div>
            </div>
            <div className='flex justify-between items-center'>
              <span className={`${archivo.className} text-[#f5f5f599] text-[16px] font-[300] tracking-[-0.16px]`}>
                Subtotal
              </span>
              <span className={`${archivo.className} text-[#F5F5F5] text-[16px] font-[400] tracking-[-0.16px]`}>
                $20.5
              </span>
            </div>
            <div className='flex items-center gap-1 cursor-pointer h-[40px] pb-3'>
              <span
                className={`${archivo.className} text-[#f5f5f599] text-[16px] font-[300] tracking-[-0.16px] hover:text-[#C5D8EE]`}
                style={{ transition: 'all 0.3s ease' }}
              >
                Add Promotion Code
              </span>
              <Image src={IconAdd} alt='IconAdd' width={16} height={16} />
            </div>
            <div className='h-[1px] w-full bg-[#998dc63d]' />
            <div className='flex justify-between items-center pt-[12px]'>
              <span className={`text-[#f5f5f599] text-[16px] font-[400] tracking-[-0.16px]`}>Total</span>
              <span className={`${archivo.className} text-[#F5F5F5] text-[18px] font-[400] tracking-[-0.18px]`}>
                $20.5
              </span>
            </div>
          </div>
          <Button
            variant='outline'
            className='w-full h-[48px] p-0 border-[1px] border-[#8A66D8] bg-transparent rounded-[4px] hover:bg-transparent mb-3 z-50 button_try_now'
            onClick={() => onChangeQR()}
          >
            <span className='text-[18px] font-[500] tracking-[-0.18px] text-[#F5F5F5]'>Checkout</span>
          </Button>
        </div>
      </div>
    </motion.div>
  )
}
