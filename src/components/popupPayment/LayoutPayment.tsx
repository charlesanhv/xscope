'use client'
import React, { useEffect, useState } from 'react'
import IconTryNow from '../../../public/images/Home/icon_try_now.svg'
import IconTryNowHover from '../../../public/images/App/icon_arrow_hover.svg'
import bgHoverPaymentItem from '../../../public/images/App/bg_hover_payment_item.svg'
import bgPaymentItem from '../../../public/images/App/bg_payment_item.svg'
import logoEth from '../../../public/images/App/logo_eth.svg'
import logoBtc from '../../../public/images/App/logo_btc.svg'
import logoSol from '../../../public/images/App/logo_sol.svg'
import iconMastercard from '../../../public/images/App/icon_mastercard.svg'
import iconVisa from '../../../public/images/App/icon_visa.svg'
import { archivo } from '@/app/font';
import '../style/PopupPayment.css'
import Image from 'next/image';
import { motion } from 'framer-motion'
import LayoutPayCoin from './LayoutPayCoin'
import LayoutQrCoin from './LayoutQrCoin'
import LayoutPaymentSuccess from './LayoutPaymentSuccess'
const LAYOUT = {
    coin: 'coin',
    card: 'card'
}
export default function LayoutPayment({ onChangeLayout }: any) {
    const [LayoutDefault, setLayoutDefault] = useState(true)
    const [changeLayout, setChangeLayout] = useState('')
    const [QrLayout, setQrLayout] = useState(false)
    const [paymentSucc,setPaymenSucc] = useState(false)
    const spring = {
        // type: 'spring',
        // damping: 10,
        // stiffness: 100
        ease: 'easeInOut',
        duration: 0.5
    }

    useEffect(() => {
        if(!QrLayout) return;

        const timeout = setTimeout(() => {
            setPaymenSucc(true)
            setQrLayout(false)
        },5000)

        return () => clearTimeout(timeout)
    },[QrLayout])

    return (
        <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} transition={spring} className='flex flex-col gap-5 z-[300]'>
            {QrLayout || paymentSucc ? null : (
                <div className='pl-6 flex items-center gap-1 pr-3 clear-start cursor-pointer btn_back relative' onClick={() => {
                    !LayoutDefault ? setLayoutDefault(true) : onChangeLayout()
                }}>
                    <Image src={IconTryNow} alt="logo" style={{ transform: 'rotate(180deg)', marginTop: 2 }} width={16} height={16} className='icon' />
                    <Image src={IconTryNowHover} alt="logo" className='icon_hover absolute' />
                    <span className={`${archivo.className} text-[#f5f5f599] text-[16px] font-[300] tracking-[0.32px] hover:text-[#C5D8EE]`} style={{ transition: 'all 0.3s ease' }}>Go Back</span>
                </div>
            )}
            {LayoutDefault ? (
                <div className='max-w-[446px] h-438px mx-auto flex flex-col gap-6 px-6 pb-6'>
                    <span className='text-[#F5F5F5] text-[24px] font-[500] tracking-[0.32px] '>
                        Choose payment method :
                    </span>
                    <div className='flex flex-col gap-3'>
                        <div className='btnPaymentItem w-full h-[104px] flex items-center justify-center cursor-pointer relative' onClick={() => {
                            setChangeLayout(LAYOUT.coin)
                            setLayoutDefault(false)
                        }}>
                            <div className='z-[200] flex flex-col items-center gap-[8px]'>
                                <span className={`${archivo.className} text-[#f5f5f5] text-[16px] font-[600]`}>
                                    Use Coin
                                </span>
                                <div className='flex items-center justify-center' style={{ transformStyle: 'preserve-3d' }}>
                                    <Image src={logoEth} alt='logoEth' className='inline-block overflow-hidden' style={{ transform: 'rotateY(-1deg)' }} />
                                    <Image src={logoBtc} alt='logoBtc' className='inline-block overflow-hidden' style={{ transform: 'rotateY(-1deg)', marginLeft: '-8px' }} />
                                    <Image src={logoSol} alt='logoSol' className='inline-block overflow-hidden' style={{ transform: 'rotateY(-1deg)', marginLeft: '-8px' }} />
                                </div>
                            </div>
                            <Image src={bgPaymentItem} alt='bgPaymentItem' className='absolute w-full h-full top-0 left-0 bgPaymentItem' />
                            <Image src={bgHoverPaymentItem} alt='bgHoverPaymentItem' className='absolute w-full h-full top-0 left-0 bgHoverPaymentItem' />
                        </div>
                        <div className='btnPaymentItem w-full h-[104px] flex items-center justify-center cursor-no-drop relative opacity-60'>
                            <div className='z-[200] flex flex-col items-center gap-[8px]'>
                                <span className={`${archivo.className} text-[#f5f5f5] text-[16px] font-[600]`}>
                                    Use Credit Card
                                </span>
                                <div className='flex items-center justify-center gap-2'>
                                    <Image src={iconMastercard} alt='iconMastercard' />
                                    <Image src={iconVisa} alt='iconVisa' />
                                </div>
                            </div>
                            <Image src={bgPaymentItem} alt='bgPaymentItem' className='absolute w-full h-full top-0 left-0' />
                            {/* <Image src={bgHoverPaymentItem} alt='bgHoverPaymentItem' className='absolute w-full h-full top-0 left-0 bgHoverPaymentItem' /> */}
                        </div>
                    </div>
                </div>
            ) : null}
            {changeLayout === LAYOUT.coin && !LayoutDefault && !QrLayout && !paymentSucc ? (
                <LayoutPayCoin onChangeQR={() => setQrLayout(true)} />
            ) : null}
            {changeLayout === LAYOUT.coin && QrLayout ? (
                <LayoutQrCoin />
            ) : null}
            {paymentSucc ? (
                <LayoutPaymentSuccess layout={changeLayout} />
            ):null}
        </motion.div>
    );
}
