'use client'
import React from 'react'
import { archivo } from '@/app/font';
import IconCoin from '../../../public/images/App/icon_coin.svg'
import Billing from '../../../public/images/App/Billing.png'
import ExampleQR from '../../../public/images/App/Example-QR.svg'
import '../style/PopupPayment.css'
import Image from 'next/image';
import { motion } from 'framer-motion'
import CountdownTimer from '../common/CountdownTimer';
export default function LayoutQrCoin() {
    const spring = {
        // type: 'spring',
        // damping: 10,
        // stiffness: 100
        ease: 'easeInOut',
        duration: 0.5
    }
    return (
        <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} transition={spring} className='flex flex-col items-center justify-center gap-10 z-[300] mx-[-24px] pt-[48px] pb-[93px]'>
            <div className='flex gap-10 px-6'>
                <Image src={ExampleQR} alt="logo" />
                <div className='w-[394px]'>
                    <div className='flex w-full justify-between'>
                        <div className='flex flex-col h-[72px] justify-center'>
                            <span className={`${archivo.className} text-[16px] font-[300] tracking-[-0.16px] text-[#f5f5f599]`}>Amount Remaining</span>
                            <span className={`${archivo.className} text-[24px] font-[500] tracking-[-0.24px] text-[#F5F5F5]`}>0.00024 BTC</span>
                        </div>
                        <div className='w-[72px] h-[72px] flex items-center justify-center rounded-[72px] border border-[#8A66D8]' style={{ background: 'linear-gradient(180deg, rgba(112, 73, 194, 0.60) 0%, rgba(112, 73, 194, 0.00) 100%)', boxShadow: '0px 0px 4px 24px 0px rgba(110, 59, 219, 0.60)' }}>
                            <span className={`${archivo.className} text-[18px] font-[400] tracking-[-0.18px] text-[#F5F5F5]`}>
                                <CountdownTimer initialTime={60} type='no_hour' />
                            </span>
                        </div>
                    </div>
                    <div className='flex flex-col h-[72px] pt-3 pb-[24px] justify-center'>
                        <span className={`${archivo.className} text-[16px] font-[300] tracking-[-0.16px] text-[#f5f5f599]`}>Address</span>
                        <span className={`${archivo.className} text-[16px] font-[400] tracking-[-0.16px] text-[#F5F5F5]`}>x07sus69q7365r398rya89dasy9dsdas7dtba7dtba</span>
                    </div>
                    <span className={`${archivo.className} text-[16px] font-[400] tracking-[-0.16px] text-[#f5f5f599]`}>Make sure to send enough to cover any transaction fees. Only send this token over the ETH Network</span>
                </div>
            </div>
            <div className='relative w-[575px] p-6'>
                <Image src={Billing} alt='Billing' className='w-full absolute top-0 left-0 right-0 bottom-0 h-full z-[-1]' />
                <div className='flex flex-col gap-3'>
                    <span className={`text-[14px] font-[400] tracking-[-0.14px] text-[#f5f5f599]`}>Order Summary</span>
                    <div className='py-3 flex gap-12'>
                        <div className='flex gap-3'>
                            <Image src={IconCoin} alt='IconCoin' width={32} height={32} />
                            <div className='flex flex-col'>
                                <span className={`${archivo.className} text-[#F5F5F5] text-[18px] font-[500] tracking-[-0.18px]`}>10 Credits</span>
                                <span className={`${archivo.className} text-[#f5f5f599] text-[16px] font-[300] tracking-[-0.16px]`}>$20.5</span>
                            </div>
                        </div>
                        <div className='flex flex-col'>
                            <span className={`${archivo.className} text-[16px] font-[400] tracking-[-0.16px] text-[#f5f5f599]`}>Transaction ID : MyLQ2nZ9A9nqNzTe</span>
                            <span className={`${archivo.className} text-[16px] font-[400] tracking-[-0.16px] text-[#f5f5f599]`}>Vertication Code : XLm23979A9nqNzTe </span>
                        </div>
                    </div>
                </div>
            </div>
        </motion.div>
    );
}
