'use client'
import React from 'react'
import { archivo } from '@/app/font';
import IconX from '../../../public/images/App/icon_x.svg'
import IconCoin from '../../../public/images/App/icon_coin.svg'
import TagPromocode from '../../../public/images/App/Tag_Promocode.png'
import logoBtc from '../../../public/images/App/logo_btc.svg'
import '../style/PopupPayment.css'
import Image from 'next/image';
import { motion } from 'framer-motion'
import '../style/PopupPayment.css'
import { Button } from '../ui/button';
import moment from 'moment'
export default function LayoutPaymentSuccess({ layout }: any) {
    const spring = {
        // type: 'spring',
        // damping: 10,
        // stiffness: 100
        ease: 'easeInOut',
        duration: 0.5
    }
    return (
        <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} transition={spring} className='px-[22px] pt-6 flex gap-10'>
            <div className='flex flex-col gap-[88px]'>
                <div className='flex flex-col gap-[12px]'>
                    <Image src={IconX} alt='logo' width={48} height={48} />
                    <div className='flex flex-col'>
                        <span className={`text-[36px] font-[400] tracking-[-0.36px] text-[#F5F5F5]`}>Payment Done</span>
                        <span className={`text-[36px] font-[400] tracking-[-0.36px] text-[#F5F5F5] text_line mt-[-8px]`}>Successfully !</span>
                    </div>
                    <span className={`text-[20px] font-[300] tracking-[-0.2px] text-[#F5F5F599]`}>Thank you for your choice</span>
                </div>
                <Button variant="outline" className="w-[204px] h-[40px] p-0 border-[1px] border-[#c5d8ee99] bg-transparent rounded-[4px] hover:bg-transparent mb-3 z-50 button_sale" >
                    <span className='text-[18px] font-[500] tracking-[-0.18px] text-[#C5D8EE]' style={{ zIndex: 100 }}>Let’s Go</span>
                </Button>
            </div>
            <div className='flex flex-col gap-3 px-6 pt-12 pb-8 w-[342px] border border-[#ffffff14] rounded-[8px]'>
                <div className='flex flex-col gap-3 pb-3'>
                    <span className='text-[#C5D8EE] text-[16px] font-[400] tracking-[0.64px]'>Billing Detail</span>
                    <span className={`${archivo.className} text-[#f5f5f599] text-[14px] font-[300]`}>{moment().format('YYYY-DD-MM, h:mm:ss A')}</span>
                </div>
                <div className='flex flex-col gap-3 pb-3'>
                    <div className='flex items-center gap-[6px]'>
                        <Image src={IconCoin} alt='IconCoin' width={24} height={24} />
                        <span className={`${archivo.className} text-[#F5F5F5] text-[18px] font-[500] tracking-[-0.18px] pl-[2px]`}>10 Credits</span>
                        <span className={`${archivo.className} text-[#f5f5f599] text-[16px] font-[300] tracking-[-0.16px]`}>$20.5</span>
                    </div>
                    <div className='flex justify-between items-center'>
                        <span className={`${archivo.className} text-[#f5f5f599] text-[16px] font-[300] tracking-[-0.16px]`}>Subtotal</span>
                        <span className={`${archivo.className} text-[#F5F5F5] text-[16px] font-[400] tracking-[-0.16px]`}>$20.5</span>
                    </div>
                    <div className='flex justify-between items-center '>
                        <div className='flex items-center gap-[6px]'>
                            <span className={`${archivo.className} text-[#f5f5f599] text-[16px] font-[300] tracking-[-0.16px]`}>Discount</span>
                            <div className='relative h-[28px] w-max flex items-center justify-end pl-[18px] pr-[8px]'>
                                <Image src={TagPromocode} alt='TagPromocode' className='absolute w-full h-full top-0 left-0 bottom-0' />
                                <span className={`${archivo.className} text-[#f5f5f599] text-[13px] font-[500] tracking-[0.26px]`}>10JQKA</span>
                            </div>
                        </div>
                        <span className={`${archivo.className} text-[#F5F5F5] text-[16px] font-[400] tracking-[-0.16px]`}>- $12.5</span>
                    </div>
                </div>
                <div className='w-full h-[1px] bg-[#998dc63d]' />
                {layout === 'coin' ? (
                    <>
                        <div className='flex flex-col gap-3 py-3'>
                            <div className='flex justify-between items-center'>
                                <span className={`${archivo.className} text-[#f5f5f599] text-[16px] font-[300] tracking-[-0.16px]`}>Transaction ID</span>
                                <span className={`${archivo.className} text-[#F5F5F5] text-[16px] font-[400] tracking-[-0.16px]`}>MyLQ2nZ9A9nqNzTe </span>
                            </div>
                            <div className='flex justify-between items-center'>
                                <span className={`${archivo.className} text-[#f5f5f599] text-[16px] font-[300] tracking-[-0.16px]`}>Address</span>
                                <span className={`${archivo.className} text-[#F5F5F5] text-[16px] font-[400] tracking-[-0.16px]`}>x07su****dtba</span>
                            </div>
                        </div>
                        <div className='w-full h-[1px] bg-[#998dc63d]' />
                        <div className='flex justify-between items-center pt-[12px]'>
                            <span className={`text-[#f5f5f599] text-[16px] font-[400] tracking-[-0.16px]`}>Total</span>
                            <div className='flex gap-2'>
                                <Image src={logoBtc} alt='IconCoin' width={24} height={24} />
                                <div className='flex flex-col items-end pt-[20px]'>
                                    <span className={`${archivo.className} text-[#f5f5f5] text-[16px] font-[400] tracking-[-0.16px]`}>0.00024 BTC</span>
                                    <span className={`${archivo.className} text-[#f5f5f599] text-[14px] font-[400] tracking-[-0.14px]`}>~ $8.24</span>
                                </div>
                            </div>
                        </div>
                    </>
                ) : null}
            </div>
        </motion.div>
    );
}
