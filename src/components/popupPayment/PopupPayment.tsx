'use client'
import React, { useState } from 'react'
import IconClose from '../../../public/images/App/icon_close.svg'
import IconCoin from '../../../public/images/App/icon_coin.svg'
import BgGetReport from '../../../public/images/App/bg-get-report.png'
import {
    DialogContent,
    DialogClose
} from "@/components/ui/dialog"
import Image from 'next/image'
import LayoutPlan from './LayoutPlan'
import LayoutPayment from './LayoutPayment'
export default function PopupPayment() {
    const [changeLayout, setChangeLayout] = useState('Layout1')
    return (
        <DialogContent className="sm:max-w-[868px] min-h-[507px] p-12 border-none" style={{ background: 'linear-gradient(0deg, rgba(255, 255, 255, 0.06) 0%, rgba(255, 255, 255, 0.06) 100%), linear-gradient(111deg, #46454E 0%, #1B1B1C 33.33%, #161616 50.5%, #211E33 100%)' }}>
            <DialogClose asChild>
                <div className='w-[40px] h-[40px] rounded-[40px] flex items-center justify-center border border-[#ffffff1f] bg-[#000] absolute right-[-40px] top-[-40px]' style={{ zIndex: 300 }} onClick={() => setChangeLayout('Layout1')}>
                    <Image src={IconClose} alt="logo" className='cursor-pointer' />
                </div>
            </DialogClose>
            <Image src={BgGetReport} alt='bg_content_item' className='absolute h-full' />
            <Image src={IconCoin} alt='IconCoin' className='absolute top-[-37px] left-[45%]' width={88} height={88} />
            {changeLayout === 'Layout2' ? (
                <LayoutPayment onChangeLayout={() => setChangeLayout('Layout1')} />
            ) : (
                <LayoutPlan onChangeLayout={() => setChangeLayout('Layout2')} />
            )}
        </DialogContent>
    );
}
