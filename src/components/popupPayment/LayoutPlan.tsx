'use client'
import React, { useState } from 'react'
import IconTick from '../../../public/images/Home/icon_tick.svg'
import Slider from '../home/Slider';
import { Button } from '../ui/button';
import { archivo } from '@/app/font';
import Image from 'next/image';
import '../style/PopupPayment.css'
import { motion } from 'framer-motion'
const data_plan = [
    {
        id: 1,
        name: 'Quantities',
        key: 'quantities',
        desc: [
            {
                id: 1,
                text: 'Lorem ipsum dolor'
            },
            {
                id: 2,
                text: 'Lorem ipsum dolor sit amet consectetur'
            },
            {
                id: 3,
                text: 'Lorem ipsum dolor sit amet'
            }
        ]
    },
    {
        id: 2,
        name: 'Plan',
        key: 'plan',
        desc: [
            {
                id: 1,
                text: 'Umlimited Credits'
            },
            {
                id: 2,
                text: 'Lorem ipsum dolor sit amet consectetur'
            },
            {
                id: 3,
                text: 'Lorem ipsum dolor sit amet'
            }
        ]
    }
]
export default function LayoutPlan({ onChangeLayout }: any) {
    const spring = {
        // type: 'spring',
        // damping: 10,
        // stiffness: 100
        ease: 'easeInOut',
        duration: 0.5
    }
    const [value, setValue] = useState(10)
    return (
        <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} transition={spring}>
            <div className='max-w-[1220px] mx-auto flex flex-col items-center gap-6 z-[300] pt-[13px]'>
                <span className='text-[32px] font-[300] tracking-[-0.32px] text-[#fefefee6]'>Buy Credit</span>
                <div className="flex gap-[32px]">
                    {data_plan?.map((item, index) => (
                        <React.Fragment key={index}>
                            <div key={index} className='flex flex-col justify-between h-[438px] rounded-[8px] p-6 relative'>
                                <div className='flex flex-col gap-3 z-50'>
                                    <span className='text-[14px] font-[500] tracking-[-0.56px] text-[#c5d8ee99]'>{item?.name}</span>
                                    {item?.key === 'plan' ? (
                                        <span className='text-[32px] font-[300] tracking-[-0.32px] text-[#fefefee6] text_enterprise'>Enterprise</span>
                                    ) : (
                                        <div>
                                            <div className='flex items-center justify-between'>
                                                <div>
                                                    <span className='text-[40px] font-[300] tracking-[-0.4px] text-[#F5F5F5]'>{value}</span>{' '}
                                                    <span className='text-[18px] font-[300] tracking-[-0.18px] text-[#F5F5F5]'>credits</span>
                                                </div>
                                                <span className='text-[20px] font-[400] tracking-[-0.2px] text-[#F5F5F5] mt-4'>${value * 2}</span>
                                            </div>
                                            <Slider min={0} max={50} step={25} defaultValue={0} onChange={(value) => {
                                                value === 0 ? setValue(10) : value === 25 ? setValue(20) : setValue(50)
                                            }} />
                                        </div>
                                    )}
                                    <div className='flex flex-col gap-3 mt-3'>
                                        {item?.desc?.map((desc, index) => (
                                            <div key={index} className='flex items-center gap-3'>
                                                <Image src={IconTick} alt="icon" />
                                                <span className={`${archivo.className} text-[16px] font-[300] tracking-[-0.32px] px-2 text-[#F5F5F5]`}>{desc?.text}</span>
                                            </div>
                                        ))}
                                    </div>
                                </div>
                                {item?.key === 'plan' ? (
                                    <Button variant="outline" className="w-full h-[44px] p-0 border-[1px] border-[#c5d8ee99] bg-transparent rounded-[4px] hover:bg-transparent mb-3 z-50 button_sale" >
                                        <span className='text-[18px] font-[500] tracking-[-0.18px] text-[#C5D8EE]' style={{ zIndex: 100 }}>Contact Sale</span>
                                    </Button>
                                ) : (
                                    <Button variant="outline" className="w-full h-[48px] p-0 border-[1px] border-[#8A66D8] bg-transparent rounded-[4px] hover:bg-transparent mb-3 z-50 button_try_now" onClick={() => onChangeLayout()}>
                                        <span className='text-[18px] font-[500] tracking-[-0.18px] text-[#F5F5F5]'>Buy Now</span>
                                    </Button>
                                )}
                            </div>
                            {index === 0 ? (
                                <div className='h-[424px] w-[1px] bg-[#998dc63d]' />
                            ) : null}
                        </React.Fragment>
                    ))}
                </div>
            </div>
        </motion.div>
    );
}
