'use client'
import Image from 'next/image'
import '../style/App.css'
import React, { useEffect, useState } from 'react'
import bg_content_item from '../../../public/images/App/bg_content_item.png'
import IconDropdown from '../../../public/images/App/icon_dropdown.svg'
import BgGetReport from '../../../public/images/App/bg-get-report.png'
import IconClock from '../../../public/images/App/icon_clock.svg'

import parse from 'html-react-parser'

interface ContentItemProps {
  itemActive: any
  timeRange: number
  setGetReport: any
  getReport: Boolean
}

import { Dialog, DialogTrigger } from '@/components/ui/dialog'
import { Button } from '@/components/ui/button'
import ModalRange from './ModalRange'
import { archivo } from '@/app/font'
import GetReportLayout from './GetReportLayout'
import { motion } from 'framer-motion'
import { DUMMY_CONTENT } from '@/constants'
import CountdownTimer from '../common/CountdownTimer'
import IconPopupContent from '../common/IconPopupContent'

const ContentItem: React.FC<ContentItemProps> = ({ itemActive, timeRange, getReport, setGetReport }) => {
  const [timerange, setTimerange] = useState(timeRange)
  const [timeError, setTimeError] = useState(false)
  const [endCountDown, setEndCount] = useState(false)

  const handleChangeTimeRange = (value: any) => {
    if (value === timerange && !endCountDown) {
      setTimeError(true)
    } else {
      setTimerange(value)
    }
  }

  const spring = {
    // type: 'spring',
    // damping: 10,
    // stiffness: 100
    ease: 'easeInOut',
    duration: 0.5
  }

  const handleGetReport = (value: number) => {
    setTimerange(value)
    setGetReport && setGetReport(true)
  }

  useEffect(() => {
    if (!timeError) return

    const timer = setTimeout(() => {
      setTimeError(false)
    }, 5000)

    return () => clearTimeout(timer)
  }, [timeError])

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={spring}
      className='flex-1 w-full max-w-[840px] h-[656px] relative p-6 z-[500]'
    >
      <Image
        src={bg_content_item}
        alt='bg_content_item'
        className='w-full h-full absolute top-0 left-0 right-0 bottom-0'
      />
      <Image
        src={BgGetReport}
        alt='BgGetReport'
        className='w-full h-full absolute top-0 left-0 right-0 bottom-0'
        style={
          !getReport
            ? { zIndex: 300, opacity: 1, transition: 'all 0.3s ease-in-out' }
            : { zIndex: -1, opacity: 0, transition: 'all 0.3s ease-in-out' }
        }
      />
      {!getReport ? (
        <div
          className={`flex items-center justify-between absolute top-0 left-0 w-full h-full`}
          style={{ zIndex: 300 }}
        >
          <GetReportLayout itemActive={itemActive} onGetReport={handleGetReport} />
        </div>
      ) : null}
      {itemActive ? (
        <>
          <div
            className='pt-6 pr-4 pl-3 flex items-start justify-between z-200'
            style={{ opacity: !getReport ? 0.6 : 1 }}
          >
            <div className='flex items-start gap-3 max-w-[453px]'>
              <Image
                src={itemActive?.logo}
                alt={itemActive?.user_name || ''}
                width={40}
                height={40}
                className='rounded-[40px]'
              />
              <div className='flex flex-col'>
                <span className='text-[20px] font-[300] tracking-[-0.2px] text-[#fafafae6] overflow-hidden text-base line-clamp-1'>
                  {itemActive?.name}
                </span>
                <span className='text-[16px] font-[300] tracking-[0.32px] text-[#fafafae6] opacity-60'>
                  @{itemActive?.user_name}
                </span>
              </div>
            </div>
            <div className='flex gap-3 flex-col justify-end items-end'>
              <span className='text-[14px] font-[400] tracking-[-0.14px] text-[#C5D8EE] opacity-60'>Time range</span>
              <Dialog>
                <DialogTrigger asChild>
                  <Button
                    variant='outline'
                    className='h-[32px] pr-3 pl-2 border-[1px] border-[#998dc64d] bg-transparent rounded-[4px] hover:bg-transparent z-50 flex gap-2 items-center'
                  >
                    <Image src={IconDropdown} alt='IconDropdown' />
                    <span className='text-[16px] font-[300] tracking-[0.32px] text-[#F5F5F5]'>{timerange}D</span>
                  </Button>
                </DialogTrigger>
                <ModalRange itemSelected={timerange} changeLink={false} onChange={handleChangeTimeRange} />
              </Dialog>
            </div>
          </div>
          {getReport ? (
            <div className='z-[300] pl-3 flex gap-[12px] items-center'>
              <div
                className='flex items-center gap-2 w-max h-[32px] bg-[#282828] rounded-[6px] border border-transparent px-4'
                style={{ transition: 'all 0.3s ease-in-out', borderColor: timeError ? '#AF1F4A' : 'transparent' }}
              >
                <Image src={IconClock} alt='IconClock' />
                <span
                  className={`${archivo.className} text-[14px] font-[400] tracking-[0.28px] text-center text-[#f5f5f599]`}
                >
                  Next {timerange}D report in :
                </span>
                <span
                  className={`${archivo.className} text-[14px] font-[400] tracking-[0.28px] text-center text-[#C5D8EE]`}
                >
                  <CountdownTimer initialTime={10000} onEndCountDown={() => setEndCount(true)} type={''} />
                </span>
              </div>
              {timeError ? (
                <span
                  className={`${archivo.className} text-[14px] font-[400] tracking-[0.28px] text-center text-[#C5D8EE]`}
                >
                  You can’t get another {timerange === 1 ? '1 day' : `${timerange} days`} recap when time is still
                  running
                </span>
              ) : null}
            </div>
          ) : null}
          <div className=' pt-[40px] pl-[12px] z-200'>
            <div
              className={`${archivo.className} max-h-[438px] pr-[56px] text-[16px] font-[300] tracking-[0.32px] text-[#fafafae6] relative div_content overflow-y-scroll`}
              style={{ zIndex: 50 }}
            >
              {!getReport ? (
                parse(DUMMY_CONTENT)
              ) : (
                <div className='flex flex-col gap-[16px]'>
                  {itemActive?.content?.map((item: any, index: number) => (
                    <span key={index} style={{ display: 'inline-block' }}>
                      {item}
                      {index === 0 ? null : <IconPopupContent content={''} itemActive={itemActive} />}
                    </span>
                  ))}
                </div>
              )}
            </div>
          </div>
        </>
      ) : null}
    </motion.div>
  )
}

export default ContentItem
