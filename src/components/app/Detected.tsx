'use client'
import '../style/App.css'
import React from 'react'
import { archivo } from '@/app/font'
import ImageGroup from '../common/ImageGroup'
import { DUMMY_LIST_IMAGE, LIST_DETECT_ITEM } from '@/constants'
import DetectedItem from './DetectedItem'

interface DetectedProps {
  getReport: Boolean
}

const Detected: React.FC<DetectedProps> = ({ getReport }) => {
  return (
    <div className='w-[238px] h-full relative pt-12 pb-10 flex flex-col gap-6'>
      <span className='text-[14px] font-[400] tracking-[-0.14px] text-[#C5D8EE]'>Detected in report</span>

      {!getReport ? (
        <div className='flex flex-col gap-3'>
          <ImageGroup listImage={DUMMY_LIST_IMAGE} />
          <span
            className={`${archivo.className} text-[16px] font-[400] tracking-[-0.16px] text-[#f5f5f599] opacity-60`}
          >
            You must get reported to see full information
          </span>
        </div>
      ) : (
        <div className='mt-6 p-[1px] pr-2 h-[499px] overflow-y-auto custom_scroll'>
          {(LIST_DETECT_ITEM || []).map((item, index) => (
            <DetectedItem item={item} key={index} />
          ))}
        </div>
      )}
    </div>
  )
}

export default Detected
