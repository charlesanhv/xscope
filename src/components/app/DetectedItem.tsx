import Image from 'next/image'
import React from 'react'
import { Tooltip, TooltipContent, TooltipProvider, TooltipTrigger } from '@/components/ui/tooltip'
import PopupInfo from '../common/PopupInfo'

import { TYPE_DETECT_ITEM } from '@/constants'
interface DetectedItemProps {
  item: any
}

const DetectedItem: React.FC<DetectedItemProps> = ({ item }) => {
  const dataDisplay = {
    image: item.avatar || item.logo || '',
    name: item.name || '',
    subName: item.primary_job_title || item.symbol || '',
    type: item.type || TYPE_DETECT_ITEM.type_1,
    price: item.price || '',
    change_24: item.change_24 || '',
    description: item.description || '',
    tags: item.sector || [],
    number_of_invests: item.number_of_invests || 0,
    total_invests: item.total_invests || 0
  }

  return (
    <TooltipProvider delayDuration={300}>
      <Tooltip>
        <TooltipTrigger asChild>
          <div className='p-3 pr-6 mb-3 h-[68px] flex items-center gap-x-3  last:mb-0 outline outline-1 outline-transparent rounded-lg hover:outline-[#998dc63d] hover:border-border_rgb-primary hover:cursor-pointer transition-all duration-100 background_rgb_primary'>
            <Image
              onError={(e) => {
                e.currentTarget.src = '/images/App/logo_twitter_dummy.svg'
              }}
              src={dataDisplay.image}
              alt='Image'
              width={32}
              height={32}
              style={{ borderRadius: '50%' }}
            />
            <div>
              <p className='line-clamp-1 text-[#F5F5F5] text-[14px] font-[300]'>{dataDisplay.name}</p>
              <p className='line-clamp-1 text-[#fafafae6] text-[14px] font-[300] opacity-60'>{dataDisplay.subName}</p>
            </div>
          </div>
        </TooltipTrigger>

        <TooltipContent
          side='left'
          sideOffset={13}
          className='background_rgb_popup_info backdrop-blur-[20px] rounded-[8px] border-[#998dc61f] w-[378px] min-h-[300px] p-6 z-[600]'
        >
          <PopupInfo data={dataDisplay} />
        </TooltipContent>
      </Tooltip>
    </TooltipProvider>
  )
}

export default DetectedItem
