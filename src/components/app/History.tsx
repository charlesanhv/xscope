'use client'
import { LIST_DATA_TWITTER } from '@/constants'
import IconArrowShow from '../../../public/images/App/icon_arrow_show.svg'
import Vector_745 from '../../../public/images/App/Vector_745.svg'
import Image from 'next/image'
import React, { useMemo, useState } from 'react'
import Link from 'next/link'
import '../style/App.css'

interface HistoryProps {
    itemActive: any;
}

const History: React.FC<HistoryProps> = ({ itemActive }) => {
    const [dataHistory] = useState(LIST_DATA_TWITTER?.slice(0, 5))
    const [showList, setShowList] = useState(false)

    const dataRenderHistory = useMemo(() => {
        let data = [...dataHistory]
        if (itemActive && !data.some(item => item.slug === itemActive.slug)) {
            data = [...data, itemActive]
        }
        return data;
    }, [itemActive, dataHistory])

    return (
        <div className={`w-full max-w-[342px] flex flex-col gap-4 relative ${dataHistory?.length < 7 ? null : 'history'} ${!showList ? 'hidden_history' : null}`} style={{ transition: 'all 0.3s ease' }}>
            <div className={`flex items-center ${showList ? 'justify-between' : 'gap-[10px]'}`}>
                <span className="text-[#C5D8EE] font-[400] text-[14px] tracking-[-0.14px] opacity-60" style={{ transition: 'all 0.3s ease', opacity: showList ? 1 : 0, width: showList ? 'auto' : 0 }}>History</span>
                <div className='relative'>
                    <div className='w-8 h-8 flex items-center justify-center border rounded-[4px] border-[#998dc63d] cursor-pointer hover:bg-[#998dc61f] button_show_list' style={{ transition: 'all 0.3s ease' }} onClick={() => setShowList(!showList)}>
                        <Image src={IconArrowShow} alt={'IconArrowShow'} width={15} height={15} className={`${showList ? 'rotate-90' : 'rotate-[-90deg]'}`} style={{ transition: 'all 0.3s ease' }} />
                    </div>
                    <div className='w-max h-[32px] bg-[#0C0C0C] absolute top-[48px] left-[-35px] px-3 rounded-[4px] z-[100] flex items-center justify-center popup_show_list'>
                        <span className="text-[14px] font-[400] tracking-[-0.14px] text-[#F5F5F5]">
                            {showList ? 'Hide List' : 'Show List'}
                        </span>
                        <Image src={Vector_745} alt={'Vector_745'} className='absolute right-[40%] top-[-10px]' />
                    </div>
                </div>
            </div>
            <div className="flex flex-col gap-1 max-h-[564px] overflow-y-scroll relative listHistory">
                {dataRenderHistory?.map((item, index) => {
                    return (
                        <React.Fragment key={index}>
                            {item?.slug === itemActive?.slug ? (
                                <div key={index} className="flex gap-4 items-start py-3 pl-3 pr-6 cursor-pointer w-full rounded-[8px] h-[68px] itemHistoryActive">
                                    <Image src={item?.logo} alt={item?.user_name} width={28} height={28} className="rounded-[28px]" />
                                    <div className={`flex flex-col ${showList ? 'opacity-100' : 'opacity-0'}`} style={{ transition: 'all 0.3s ease' }}>
                                        <span className="text-[16px] font-[300] tracking-[-0.16px] text-[#fafafae6] overflow-hidden text-base line-clamp-1">{item?.name}</span>
                                        <span className="text-[16px] font-[300] tracking-[0.32px] text-[#fafafa99]">@{item?.user_name}</span>
                                    </div>
                                </div>
                            ) : (
                                <Link href={`/app/recap/${item?.slug}?time=3`} className="flex gap-4 items-start p-3 cursor-pointer w-full rounded-[8px] h-[68px] item_list_twit">
                                    <Image src={item?.logo} alt={item?.user_name} width={28} height={28} className="rounded-[28px]" />
                                    <div className={`flex flex-col ${showList ? 'opacity-100' : 'opacity-0'}`} style={{ transition: 'all 0.3s ease' }}>
                                        <span className="text-[16px] font-[300] tracking-[-0.16px] text-[#fafafae6] overflow-hidden text-base line-clamp-1">{item?.name}</span>
                                        <span className="text-[16px] font-[300] tracking-[0.32px] text-[#fafafa99]">@{item?.user_name}</span>
                                    </div>
                                </Link>
                            )
                            }
                        </React.Fragment>
                    )
                })}
            </div>
        </div>
    );
}

export default History
