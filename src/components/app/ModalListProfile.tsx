'use client'
import React, { useRef, useState } from 'react'
import '../../components/style/ModalListProfile.css'
import IconClose from '../../../public/images/App/icon_close.svg'
import IconTwitter from '../../../public/images/App/icon_Twittericon_Twitter.svg'
import IconSearchInput from '../../../public/images/App/icon_search_input.svg'
import IconTryNow from '../../../public/images/Home/icon_try_now.svg'
import { DialogContent, DialogClose } from '@/components/ui/dialog'
import Link from 'next/link'
import Image from 'next/image'
import { archivo } from '../../app/font'
import { LIST_DATA_TWITTER } from '@/constants'
import IconFire from '../common/IconFire'
export default function ModalListProfile({ type, onGetDataModal, listProfile = [], onEditListProfile }: any) {
  const inputRef = useRef<HTMLInputElement>(null)
  const [valueSearch, setValueSearch] = useState('')
  const [itemSelected, setItemSelected] = useState<any[]>([])
  const [listDataRemove, setListDataRemove] = useState<any[]>([])
  const [editMode, setEditMode] = useState(false)

  const handleCheckboxChange = (item: object) => {
    const itemIndex = itemSelected.indexOf(item)

    if (itemIndex === -1) {
      setItemSelected([...itemSelected, item])
    } else {
      const updatedSelectedItems = [...itemSelected]
      updatedSelectedItems.splice(itemIndex, 1)
      setItemSelected(updatedSelectedItems)
    }
  }

  const handleRemoveItem = (item: object) => {
    if (!item) return
    const itemIndex = listDataRemove.indexOf(item)
    if (itemIndex === -1) {
      setListDataRemove([...listDataRemove, item])
    } else {
      const updatedSelectedItems = [...listDataRemove]
      updatedSelectedItems.splice(itemIndex, 1)
      setListDataRemove(updatedSelectedItems)
    }
  }

  const hanleSaveEditData = () => {
    onEditListProfile && onEditListProfile(listDataRemove)
    setEditMode(false)
  }

  const renderLayout = (type: string) => {
    let html = null
    switch (type) {
      case 'following':
        html = (
          <div className='w-full max-w-[800px] flex flex-col gap-10'>
            <div className='w-full flex flex-col items-center justify-between gap-2'>
              <Image src={IconTwitter} alt='IconTwitter' className='cursor-pointer' />
              <span className='text-[14px] font-[400] tracking-[-0.14px] text-[#C5D8EE] pt-1'>
                Add from your Following
              </span>
              <div className='flex gap-[6px] items-end'>
                <span className={`${archivo.className} text-[20px] font-[400] tracking-[0.2px] text-[#F5F5F5]`}>
                  1,429
                </span>
                <span className={`${archivo.className} text-[16px] font-[300] tracking-[0.32px] text-[#F5F5F599]`}>
                  following
                </span>
              </div>
            </div>
            <div
              className='bg-[#1D1D20] rounded-[6px] h-[56px] w-full pl-[12px] pr-[24px] flex items-center justify-center cursor-pointer border border-transparent hover:border-[#c5d8ee4d] input_modal_list_profile'
              onClick={() => inputRef?.current && inputRef?.current.focus()}
            >
              <Image src={IconSearchInput} alt='IconSearchInput' />
              <input
                placeholder='Search by user name'
                ref={inputRef}
                value={valueSearch}
                onChange={(e) => setValueSearch(e.target.value)}
                className='w-auto outline-none text-center bg-transparent border-none h-full text-[#F5F5F5] text-[16px] font-[300] tracking-[-0.16px]'
              />
            </div>
            <div className='w-full flex flex-col gap-1 max-h-[378px] overflow-y-scroll list_data'>
              {LIST_DATA_TWITTER?.filter((item: any) => {
                return item?.name?.toLowerCase().includes(valueSearch?.toLowerCase())
              })?.map((item: any, index: any) => {
                return (
                  <div
                    key={index}
                    className='flex gap-4 items-start p-3 cursor-pointer w-full rounded-[8px] item_list_twit'
                    onClick={() => handleCheckboxChange(item)}
                  >
                    <label className='custom-checkbox'>
                      <input
                        type='checkbox'
                        checked={itemSelected.includes(item)}
                        onChange={() => handleCheckboxChange(item)}
                      />
                      <span className='checkmark'></span>
                    </label>
                    <Image
                      src={item?.profile_image_url_https ? item?.profile_image_url_https : item?.logo}
                      alt={item?.screen_name ? item?.screen_name : item?.user_name}
                      width={32}
                      height={32}
                      className='rounded-[32px]'
                    />
                    <div className='flex flex-col'>
                      <div className='flex items-center gap-2 relative'>
                        <span className='text-[16px] font-[300] tracking-[-0.16px] text-[#fafafae6]'>{item?.name}</span>
                        <div className='absolute right-[-40px]'>
                          <IconFire />
                        </div>
                      </div>
                      <span
                        className={`${archivo.className} text-[16px] font-[300] tracking-[-0.16px] text-[#f5f5f599]`}
                      >
                        @{item?.screen_name ? item?.screen_name : item?.user_name}
                      </span>
                    </div>
                  </div>
                )
              })}
            </div>
            <DialogClose
              className='flex justify-center w-[218px] h-[48px] items-center border-[1px] border-[#8A66D8] rounded-[4px] mx-auto pr-3 pl-6 cursor-pointer button_add_list'
              onClick={() => onGetDataModal && onGetDataModal(itemSelected)}
            >
              <span className='flex-1 text-center text-[#F5F5F5] text-[18px] font-[500] tracking-[-0.18px]'>
                Add to list
              </span>
              <Image src={IconTryNow} alt='logo' style={{ zIndex: '100' }} />
            </DialogClose>
          </div>
        )
        break
      case 'your_profile':
        html = (
          <>
            <div className='p-6 flex flex-col gap-8'>
              <div className='w-full flex gap-2 justify-between items-end'>
                <div>
                  <span className='text-[24px] font-[400] tracking-[-0.24px] text-[#C5D8EE]'>Your List</span>
                  <div className='flex items-end gap-[6px]'>
                    <span className={`${archivo.className} text-[20px] font-[400] tracking-[0.2px] text-[#F5F5F5]`}>
                      {listProfile?.length}
                    </span>
                    <span className={`${archivo.className} text-[16px] font-[300] tracking-[0.32px] text-[#F5F5F599]`}>
                      profiles
                    </span>
                  </div>
                </div>
                <div className='flex items-center gap-2 text-[14px] font-[400] tracking-[0.28px] text-[#F5F5F599] cursor-pointer'>
                  {editMode ? (
                    <>
                      <span className='hover:text-[#C5D8EE] opacity-60' onClick={() => hanleSaveEditData()}>
                        Save
                      </span>
                      <span className='hover:text-[#C5D8EE] opacity-60' onClick={() => setEditMode(false)}>
                        Cancel
                      </span>
                    </>
                  ) : (
                    <span className='hover:text-[#C5D8EE] opacity-60' onClick={() => setEditMode(true)}>
                      Edit
                    </span>
                  )}
                </div>
              </div>
              <div className='w-full flex flex-col gap-1 min-h-[214px] max-h-[478px] overflow-y-scroll list_data'>
                {listProfile.map((item: any, index: any) => {
                  return (
                    <div
                      key={index}
                      className='flex gap-4 items-start p-3 cursor-pointer w-full rounded-[8px] item_list_twit'
                      style={{ transition: 'all 0.3s ease' }}
                      onClick={() => handleRemoveItem(editMode ? item : null)}
                    >
                      <label className={`${editMode ? 'custom-checkbox' : 'custom-checkbox-hidden'}`}>
                        <input
                          type='checkbox'
                          style={{ display: !editMode ? 'none' : 'block' }}
                          checked={listProfile.includes(item) && !listDataRemove.includes(item)}
                          onChange={() => handleCheckboxChange(item)}
                        />
                        <span className='checkmark' style={{ display: !editMode ? 'none' : 'block' }}></span>
                      </label>
                      <Image
                        src={item?.profile_image_url_https ? item?.profile_image_url_https : item?.logo}
                        alt={item?.screen_name ? item?.screen_name : item?.user_name}
                        width={32}
                        height={32}
                        className='rounded-[32px]'
                      />
                      <div className='flex flex-col'>
                        <div className='flex items-center gap-2 relative'>
                          <span className='text-[16px] font-[300] tracking-[-0.16px] text-[#fafafae6]'>
                            {item?.name}
                          </span>
                          <div className='absolute right-[-40px]'>
                            <IconFire />
                          </div>
                        </div>
                        <span
                          className={`${archivo.className} text-[16px] font-[300] tracking-[-0.16px] text-[#f5f5f599]`}
                        >
                          @{item?.screen_name ? item?.screen_name : item?.user_name}
                        </span>
                      </div>
                    </div>
                  )
                })}
              </div>
            </div>
            <div className='px-4 pb-4 flex flex-col gap-8'>
              <div className='h-[1px] w-full bg-[#998dc63d]' />
              <div className='flex gap-3 w-full justify-end'>
                <DialogClose className='w-[218px] h-[48px] p-0 border-[1px] border-[#c5d8ee99] bg-transparent rounded-[4px] button_cancel'>
                  <Link href={'/recap'}>
                    <span className='flex-1 text-center text-[#F5F5F5] text-[18px] font-[500] tracking-[-0.18px]'>
                      Cancel
                    </span>
                  </Link>
                </DialogClose>
                <DialogClose>
                  <Link
                    href={'/app/recap'}
                    className={`flex justify-center h-[48px] items-center border-[1px] border-[#8A66D8] rounded-[4px] gap-4 mx-auto pr-3 pl-6 cursor-pointer ${editMode || listProfile?.length === 0 ? 'button_disabled' : 'button_add_list'}`}
                  >
                    <span className='flex-1 text-center text-[#F5F5F5] text-[18px] font-[500] tracking-[-0.18px]'>
                      Go to Recap Page
                    </span>
                    <Image src={IconTryNow} alt='logo' style={{ zIndex: '100' }} />
                  </Link>
                </DialogClose>
              </div>
            </div>
          </>
        )
        break
    }
    return html
  }
  return (
    <DialogContent
      className={`border-none w-full max-w-[800px] ${type === 'following' ? 'px-10 py-12' : 'p-6'}`}
      style={{
        background:
          'linear-gradient(0deg, rgba(255, 255, 255, 0.06) 0%, rgba(255, 255, 255, 0.06) 100%), linear-gradient(111deg, #46454E 0%, #1B1B1C 33.33%, #161616 50.5%, #211E33 100%)',
        zIndex: 200
      }}
    >
      {renderLayout(type)}
      <DialogClose asChild>
        <div
          className='w-[40px] h-[40px] flex items-center justify-center absolute top-[4px] right-[4px] bg-[rgb(39 39 46)]'
          style={{ zIndex: 300 }}
        >
          <Image src={IconClose} alt='logo' className='cursor-pointer' />
        </div>
      </DialogClose>
    </DialogContent>
  )
}
