'use client'
import '../style/App.css'
import React, { useState } from 'react'
import IconToken from '../../../public/images/App/icon_token.svg'
import DummyImage from '../../../public/images/App/dummy-image.svg'
import Image from 'next/image'
import { Button } from '../ui/button'
import { archivo } from '@/app/font'
import { LIST_DATA_TWITTER } from '@/constants'

const timeRange = [
    { value: 1, label: '1D' },
    { value: 3, label: '3D' },
    { value: 7, label: '7D' },
    { value: 30, label: '30D' },
]

interface GetReportLayoutProps {
    itemActive: any,
    onGetReport: any
}

const GetReportLayout: React.FC<GetReportLayoutProps> = ({ itemActive, onGetReport }) => {
    const [itemSelected, setItemSelected] = useState<number>()
    return (
        <div className={`w-[491px] mx-auto relative flex flex-col gap-12 ${itemActive ? 'w-[321px]' : 'w-[491px]'}`}>
            {itemActive ? null : (
                <div className='flex flex-col justify-center items-center'>
                    <div className='flex items-center justify-center' style={{transformStyle: 'preserve-3d'}}>
                        <span className={`${archivo.className} text-[16px] font-[500] tracking-[-0.16px] text-center text-[#F5F5F5] w-[32px] h-[32px] rounded-[16px] bg-[#8A66D8] flex items-center justify-center`}>{LIST_DATA_TWITTER?.length - 3}</span>
                        {LIST_DATA_TWITTER?.slice(0, 3).map((item, index) => (
                            <div key={index} className='rounded-[24px] inline-block overflow-hidden' style={{transform:'rotateY(-1deg)',marginLeft: '-4px'}}>
                                <Image src={DummyImage} alt='logo' width={24} height={24} />
                            </div>
                        ))}
                    </div>
                    <span className='text-[40px] font-[300] tracking-[-0.4px] text-center text-[#F5F5F5]' style={{ textShadow: '0px 0px 16px rgba(255, 255, 255, 0.16)' }}>Get Report for All Profiles</span>
                </div>
            )}
            <div className='flex flex-col gap-5 justify-center items-center'>
                <span className='text-[20px] font-[400] tracking-[-0.2px] text-center text-[#F5F5F5]' style={{ textShadow: '0px 0px 16px rgba(255, 255, 255, 0.16)' }}>Select Time Range</span>
                <div className='flex justify-center gap-2'>
                    {timeRange?.map((item, index) => (
                        <React.Fragment key={index}>
                            {itemSelected === item?.value ? (
                                <div className='px-3 border border-[#8174B0] rounded-[4px] h-[32px] flex items-center cursor-pointer bg-[#1C1C1C]' style={{ transition: 'all 0.3s ease' }}>
                                    <span className='text-[16px] text-[#F5F5F5] font-[400] tracking-[0.32px]'>{item?.label}</span>
                                </div>
                            ) : (
                                <div className='px-3 border border-[#998dc64d] rounded-[4px] h-[32px] flex items-center cursor-pointer hover:bg-[#998dc61f]' style={{ transition: 'all 0.3s ease' }} onClick={() => setItemSelected(item?.value)}>
                                    <span className='text-[16px] text-[#f5f5f599] font-[400] tracking-[0.32px]'>{item?.label}</span>
                                </div>
                            )}
                        </React.Fragment>
                    ))}
                </div>
            </div>
            <div className='flex flex-col gap-4'>
                <div className='flex justify-center items-center gap-[6px]'>
                    <Image src={IconToken} alt='IconToken' />
                    <span className={`${archivo.className} text-[14px] font-[400] tracking-[0.28px] text-center text-[#C5D8EE]`}>78 credits</span>
                </div>
                <Button variant="outline" className="w-full h-[56px] p-0 border-[1px] border-[#8A66D8] bg-transparent rounded-[4px] hover:bg-transparent mb-3 z-50 button_Recap" onClick={() => {itemSelected && itemActive ? onGetReport && onGetReport(itemSelected) : null}}>
                    <span className='text-[18px] font-[500] tracking-[-0.18px] text-[#F5F5F5]' style={{ textShadow: '0px 0px 4px #8A66D8' }}>Get Report</span>
                </Button>
            </div>
        </div>
    );
}

export default GetReportLayout
