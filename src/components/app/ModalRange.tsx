'use client'
import React, { useState } from 'react'
import '../../components/style/App.css'
import IconClose from '../../../public/images/App/icon_close.svg'
import {
    DialogContent,
    DialogHeader,
    DialogTitle,
    DialogClose
} from "@/components/ui/dialog"
import Link from 'next/link'
import Image from 'next/image'

const List_range = [
    { value: 1, label: '1D' },
    { value: 3, label: '3D' },
    { value: 7, label: '7D' },
    { value: 30, label: '30D' },
]

export default function ModalRange({ itemSelected, changeLink = true, onChange }: any) {
    const [itemSelect, setItemSelect] = useState(3)
    const handleChangeItem = (value: any) => {
        setItemSelect(value)
    }
    return (
        <DialogContent className="sm:max-w-[384px] p-12 border-none" style={{ background: 'linear-gradient(0deg, rgba(255, 255, 255, 0.06) 0%, rgba(255, 255, 255, 0.06) 100%), linear-gradient(111deg, #46454E 0%, #1B1B1C 33.33%, #161616 50.5%, #211E33 100%)' }}>
            <DialogHeader>
                <DialogTitle className='text-center text-[20px] text-[#F5F5F5] font-[400] tracking-[-0.2px] text' style={{ textShadow: '0px 0px 16px rgba(255, 255, 255, 0.16)' }}>Select Time range</DialogTitle>
            </DialogHeader>
            <div className='flex justify-center gap-2 px-6 py-8 mb-10'>
                {List_range?.map((item, index) => {
                    return (
                        <React.Fragment key={index}>
                            {itemSelect === item?.value ? (
                                <div className='px-3 border border-[#8174B0] rounded-[4px] h-[32px] flex items-center cursor-pointer bg-[#1C1C1C]'>
                                    <span className='text-[16px] text-[#F5F5F5] font-[400] tracking-[0.32px]'>{item?.label}</span>
                                </div>
                            ) : (
                                <div className='px-3 border border-[#998dc64d] rounded-[4px] h-[32px] flex items-center cursor-pointer hover:bg-[#998dc61f]' onClick={() => handleChangeItem(item?.value)}>
                                    <span className='text-[16px] text-[#f5f5f599] font-[400] tracking-[0.32px]'>{item?.label}</span>
                                </div>
                            )}
                        </React.Fragment>
                    )
                })}
            </div>
            <div className='flex items-center gap-[6px] justify-center mb-4'>
                <div className='w-4 h-4 rounded-[16px] bg-[#7A7A7A]' />
                <span className='font-[300] text-[14px] tracking-[0.28px] text-[#f5f5f599]'>3 credits</span>
            </div>
            <DialogClose asChild>
                {!changeLink ? (
                    <div className="w-full h-[48px] cursor-pointer p-0 flex items-center justify-center border border-[#8A66D8] bg-transparent rounded-[4px] z-50 button_Recap" onClick={() => onChange && onChange(itemSelect)}>
                        <span className='text-[18px] font-[500] tracking-[-0.18px] text-[#F5F5F5]'>Recap</span>
                    </div>
                ) : (
                    <Link href={`/app/${itemSelected}?time=${itemSelect}`} className="w-full h-[48px] p-0 flex items-center justify-center border border-[#8A66D8] bg-transparent rounded-[4px] z-50 button_Recap">
                        <span className='text-[18px] font-[500] tracking-[-0.18px] text-[#F5F5F5]'>Recap</span>
                    </Link>
                )}
            </DialogClose>
            <DialogClose asChild>
                <div className='w-[40px] h-[40px] flex items-center justify-center absolute top-[4px] right-[4px] bg-[rgb(39 39 46)]' style={{ zIndex: 300 }}>
                    <Image src={IconClose} alt="logo" className='cursor-pointer' />
                </div>
            </DialogClose>
        </DialogContent>
    );
}
