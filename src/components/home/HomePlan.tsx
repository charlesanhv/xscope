'use client'
import Image from 'next/image'
import IconTick from '../../../public/images/Home/icon_tick.svg'
import CardQuantities from '../../../public/images/Home/card_Quantities.png'
import CardPlan from '../../../public/images/Home/card_plan.png'
import { Button } from "@/components/ui/button"
import Slider from './Slider'
import { useState } from 'react'
import { archivo } from '@/app/font'
import '../style/home.css'
import {
    Dialog,
    DialogTrigger,
} from "@/components/ui/dialog"
import PopupPayment from '../popupPayment/PopupPayment'
const data_plan = [
    {
        id: 1,
        name: 'Quantities',
        key: 'quantities',
        bg: CardQuantities,
        desc: [
            {
                id: 1,
                text: 'Lorem ipsum dolor'
            },
            {
                id: 2,
                text: 'Lorem ipsum dolor sit amet consectetur'
            },
            {
                id: 3,
                text: 'Lorem ipsum dolor sit amet'
            }
        ]
    },
    {
        id: 2,
        name: 'Plan',
        key: 'plan',
        bg: CardPlan,
        desc: [
            {
                id: 1,
                text: 'Umlimited Credits'
            },
            {
                id: 2,
                text: 'Lorem ipsum dolor sit amet consectetur'
            },
            {
                id: 3,
                text: 'Lorem ipsum dolor sit amet'
            }
        ]
    }
]
export default function HomePlan() {
    const [value, setValue] = useState(10)

    return (
        <div className="w-full py-[112px] z-50" style={{ background: 'linear-gradient(111deg, #1B1B1C 0%, #171719 37.52%, #1E1B2A 62.18%, #403F47 100%)' }}>
            <div className='max-w-[1220px] mx-auto flex items-start gap-10'>
                <div className="flex flex-col gap-[10px] max-w-[378px]">
                    <span className="text-[#998DC6] text-[20px] font-[300] tracking-[-0.2px]">
                        Plan For You
                    </span>
                    <span className="text-[#fefefee6] text-[40px] font-[250] tracking-[-0.4px] pr-6">Research with <span className="font-[500] no_limitations">no limitations</span> today</span>
                </div>
                <div className="flex gap-[24px]">
                    {data_plan?.map((item, index) => (
                        <div key={index} className='flex flex-col justify-between h-[438px] rounded-[8px] p-6 relative'>
                            <div className='flex flex-col gap-3 z-50'>
                                <span className='text-[14px] font-[500] tracking-[-0.56px] text-[#c5d8ee99]'>{item?.name}</span>
                                {item?.key === 'plan' ? (
                                    <span className='text-[32px] font-[300] tracking-[-0.32px] text-[#fefefee6]'>Enterprise</span>
                                ) : (
                                    <div>
                                        <div className='flex items-center justify-between'>
                                            <div>
                                                <span className='text-[40px] font-[300] tracking-[-0.4px] text-[#F5F5F5]'>{value}</span>{' '}
                                                <span className='text-[18px] font-[300] tracking-[-0.18px] text-[#F5F5F5]'>credits</span>
                                            </div>
                                            <span className='text-[20px] font-[400] tracking-[-0.2px] text-[#F5F5F5] mt-4'>${value * 2}</span>
                                        </div>
                                        <Slider min={0} max={50} step={25} defaultValue={0} onChange={(value) => {
                                            value === 0 ? setValue(10) : value === 25 ? setValue(20) : setValue(50)
                                        }} />
                                    </div>
                                )}
                                <div className='flex flex-col gap-3 mt-3'>
                                    {item?.desc?.map((desc, index) => (
                                        <div key={index} className='flex items-center gap-3'>
                                            <Image src={IconTick} alt="icon" />
                                            <span className={`${archivo.className} text-[16px] font-[300] tracking-[-0.32px] px-2 text-[#F5F5F5]`}>{desc?.text}</span>
                                        </div>
                                    ))}
                                </div>
                            </div>
                            <Dialog >
                                <DialogTrigger asChild>
                                    {item?.key === 'plan' ? (
                                        <Button variant="outline" className="w-full h-[44px] p-0 border-[1px] border-[#c5d8ee99] bg-transparent rounded-[4px] hover:bg-transparent mb-3 z-50 button_sale" >
                                            <span className='text-[18px] font-[500] tracking-[-0.18px] text-[#C5D8EE]' style={{ zIndex: 100 }}>Contact Sale</span>
                                        </Button>
                                    ) : (
                                        <Button variant="outline" className="w-full h-[48px] p-0 border-[1px] border-[#8A66D8] bg-transparent rounded-[4px] hover:bg-transparent mb-3 z-50 button_try_now">
                                            <span className='text-[18px] font-[500] tracking-[-0.18px] text-[#F5F5F5]'>Buy Now</span>
                                        </Button>
                                    )}
                                </DialogTrigger>
                                <PopupPayment />
                            </Dialog>
                            <Image src={item?.bg} alt='bg' className='absolute top-0 right-0 left-0 bottom-0 h-full' />
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
}
