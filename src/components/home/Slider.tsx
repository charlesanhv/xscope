/* eslint-disable no-unused-vars */

'use client'
import React, { useState } from 'react';
import '../style/home.css'
interface SliderProps {
    min: number;
    max: number;
    step: number;
    defaultValue: number;
    onChange: (value: number) => void
}
const list_label_data = [
    { value: 0, label: 10 },
    { value: 25, label: 20 },
    { value: 50, label: 50 },
]
export default function Slider({ min, max, step, defaultValue, onChange  }: SliderProps) {
    const [value, setValue] = useState(defaultValue);

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setValue(parseInt(event.target.value));
        onChange && onChange(parseInt(event.target.value))
    };
    return (
        <div>
            <input
                type="range"
                min={min}
                max={max}
                step={step}
                value={value}
                onChange={handleChange}
                className={'slider_item'} // Assign CSS class
                list="tickmarks"
            />
            <div className='flex mt-4 justify-between items-center'>
                {list_label_data?.map((item, index) => {
                    return (
                        <React.Fragment key={index}>
                            {value === item?.value ? (
                                <div className='text-[#f5f5f5] text-[18px] font-[400] tracking-[-0.18px]'>{item?.label}</div>
                            ) : (
                                <div className='text-[#f5f5f566] text-[18px] font-[400] tracking-[-0.18px]'>{item?.label}</div>
                            )}
                        </React.Fragment>
                    )
                })}
            </div>
        </div>
    );
}
