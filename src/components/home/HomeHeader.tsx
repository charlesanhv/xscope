"use client"
import Image from 'next/image'
// import Xscope from '../../../public/images/Home/Group_5596.png'
import Xscope from '../../../public/images/Home/XScope_Eff.svg'
import IconTryNow from '../../../public/images/Home/icon_try_now.svg'
import DummyAvatar from '../../../public/images/Home/avatar.svg'
import Marquee from 'react-fast-marquee'
import '../style/home.css'
import Link from 'next/link'
import { archivo } from '@/app/font'
import IconFire from '../common/IconFire'
import ModalLogin from '../common/ModalLogin'
import { useState } from 'react'
import { useAppContext } from '@/app/app-provider'
// import { ModeToggle } from '@/components/mode-toggle'

const DUMMY_DATA_CARD = [
  {
    id: 1,
    name: 'Yung Patek | Baby Thebec Desiigz',
    avatar: DummyAvatar,
    nickname: 'yung.patek',
    desc: `Design Trends: Dive into the latest data-driven insights on global markets, including stocks, cryptocurrencies, and commodities.
    Economic Indicators: Explore key economic indicators and their implications for investors and businesses worldwide.
    3️⃣ Industry Analysis: Stay informed with in-depth analysis of various industries, from technology and healthcare to finance and energy.`
  },
  {
    id: 2,
    name: 'Statistr | Hiring Data Contributor',
    avatar: DummyAvatar,
    nickname: 'yung.patek',
    desc: `Market Trends: Dive into the latest data-driven insights on global markets, including stocks, cryptocurrencies, and commodities.
    Economic Indicators: Explore key economic indicators and their implications for investors and businesses worldwide.
    3️⃣ Industry Analysis: Stay informed with in-depth analysis of various industries, from technology and healthcare to finance and energy.`
  },
  {
    id: 3,
    name: 'Yung Patek | Baby Thebec Desiigz',
    avatar: DummyAvatar,
    nickname: 'statistr_',
    desc: `Design Trends: Dive into the latest data-driven insights on global markets, including stocks, cryptocurrencies, and commodities.
    Economic Indicators: Explore key economic indicators and their implications for investors and businesses worldwide.
    3️⃣ Industry Analysis: Stay informed with in-depth analysis of various industries, from technology and healthcare to finance and energy.`
  },
  {
    id: 4,
    name: 'Yung Patek | Baby Thebec Desiigz',
    avatar: DummyAvatar,
    nickname: 'statistr_',
    desc: `Design Trends: Dive into the latest data-driven insights on global markets, including stocks, cryptocurrencies, and commodities.
    Economic Indicators: Explore key economic indicators and their implications for investors and businesses worldwide.
    3️⃣ Industry Analysis: Stay informed with in-depth analysis of various industries, from technology and healthcare to finance and energy.`
  }
]

export default function HomeHeader() {
  const { isAuthenticated } = useAppContext()
  const [isOpenModalLogin, setIsOpenModalLogin] = useState<boolean>(false)


  const handleSignin = () => {
    setIsOpenModalLogin(true)
  }

  const handleCloseModalLogin = () => {
    setIsOpenModalLogin(false)
  }

  return (
    <>
    <div
      className='w-full flex items-center gap-10 flex-col pb-[72px] z-50'
      style={{ background: 'linear-gradient(122deg, #46454E 12.27%, #1B1B1C 37.31%, #161616 50.21%, #211E33 87.39%)' }}
    >
      <div className='max-w-[1016px] mx-auto pt-[210px] flex flex-col items-center'>
        <p className='text-center text-[56px] font-[300] text-[#FAFAFAE6] tracking-[-0.56px]'>
          Get things in your hand real
          <span className='flex items-center justify-center'>
            {' '}
            <span style={{ marginBottom: '12px' }}>quick with</span>{' '}
            <Image src={Xscope} alt='logo' width={297} height={61} />{' '}
          </span>
        </p>
        <Link
          href={'/app'}
          className='flex justify-center w-[218px] h-[48px] items-center border-[1px] border-[#8A66D8] rounded-[4px] pr-3 pl-6 mb-[96px] cursor-pointer button_try_now'
          onClick={() => !isAuthenticated && handleSignin()}
        >
          <span className='flex-1 text-center text-[#F5F5F5] text-[18px] font-[500] tracking-[-0.18px]'>Try Now</span>
          <Image src={IconTryNow} alt='logo' style={{ zIndex: '100' }} />
        </Link>
      </div>
      <div className='mx-[72px] slider flex flex-col gap-10'>
        <div className='py-[24px]'>
          <Marquee style={{ position: 'relative' }} direction='right' pauseOnHover={true}>
            {DUMMY_DATA_CARD?.map((item, index) => {
              return (
                <div className='w-[432px] h-[212px] flex flex-col gap-[16px] p-6 ml-20 cardRight' key={index}>
                  <div className='borderChild' />
                  <div className='flex w-full justify-between items-start'>
                    <div className='flex items-start gap-[12px] w-[264px]'>
                      <Image
                        src={item?.avatar}
                        width={40}
                        height={40}
                        className='rounded-[40px] mt-[4px]'
                        alt={item?.nickname}
                      />
                      <div className='flex flex-col'>
                        <span className='text-[#FAFAFAE6] text-[18px] font-[300] tracking-[-0.2px] overflow-hidden text-base line-clamp-2'>
                          {item?.name}
                        </span>
                        <span className='text-[#f5f5f599] text-[16px] font-[300] tracking-[0.32px]'>
                          @{item?.nickname}
                        </span>
                      </div>
                    </div>
                    {/* <Image src={IconFire} alt="logo" /> */}
                    <div style={{ marginRight: '10px' }}>
                      <IconFire />
                    </div>
                  </div>
                  <div
                    className={`${archivo.className} text-[#FAFAFAE6] text-[16px] font-[300] tracking-[0.32px] overflow-hidden text-base line-clamp-3`}
                  >
                    {item?.desc}
                  </div>
                </div>
              )
            })}
          </Marquee>
        </div>
        <div className='py-[24px] relative'>
          <Marquee style={{ position: 'relative' }} direction='left' pauseOnHover={true}>
            {DUMMY_DATA_CARD?.map((item, index) => {
              return (
                <div className='w-[432px] h-[212px] flex flex-col gap-[16px] p-6 ml-20 cardLeft' key={index}>
                  <div className='borderChild' />
                  <div className='flex justify-between items-start'>
                    <div className='flex items-start gap-[12px] w-[264px]'>
                      <Image
                        src={item?.avatar}
                        width={40}
                        height={40}
                        className='rounded-[40px]'
                        alt={item?.nickname}
                      />
                      <div className='flex flex-col'>
                        <span className='text-[#FAFAFAE6] text-[18px] font-[300] tracking-[-0.2px] overflow-hidden text-base line-clamp-2'>
                          {item?.name}
                        </span>
                        <span className='text-[#f5f5f599] text-[16px] font-[300] tracking-[0.32px]'>
                          @{item?.nickname}
                        </span>
                      </div>
                    </div>
                    <div style={{ marginRight: '10px' }}>
                      <IconFire />
                    </div>
                  </div>
                  <div
                    className={`${archivo.className} text-[#FAFAFAE6] text-[16px] font-[300] tracking-[0.32px] overflow-hidden text-base line-clamp-3`}
                  >
                    {item?.desc}
                  </div>
                </div>
              )
            })}
          </Marquee>
        </div>
      </div>
    </div>
     <ModalLogin open={isOpenModalLogin} onClose={handleCloseModalLogin} />
     </>
  )
}
