'use client'
import React from 'react'
import { useAppContext } from '@/app/app-provider'
import './style/header.css'
// import ButtonLogout from '@/components/button-logout'
import Image from 'next/image'
// import { ModeToggle } from '@/components/mode-toggle'
import Link from 'next/link'
import Logo from '../../public/images/Header&Footer/logoXscope.svg'
import DummyUser from '../../public/images/App/icon_token.svg'
import Tab_name from '../../public/images/Home/Tab_name.png'
import ModalLogin from './common/ModalLogin'

export default function Header() {
  const dummyDataUser = {
    credits: 212,
    nameuser: 'Trann Son',
    avatar: DummyUser
  }
  const { isAuthenticated } = useAppContext()

  const [isOpenModalLogin, setIsOpenModalLogin] = React.useState<boolean>(false)

  const handleSignin = () => {
    setIsOpenModalLogin(true)
  }

  const handleCloseModalLogin = () => {
    setIsOpenModalLogin(false)
  }

  return (
    <div
      className='fixed inset-x-0 top-0 left-0 h-16 px-20  bg-bgheader z-[400]'
      style={{ background: 'linear-gradient(180deg, #171617 0%, #37373E 100%)' }}
    >
      <div className='max-w-[1222px] h-full mx-auto flex items-center justify-between'>
        <div className='flex justify-between w-max items-center gap-3'>
          <Link href='/' className='text-[16px] font-[400] tracking-[-0.16px] px-2 text-[#FAFAFAE6]'>
            {' '}
            <Image src={Logo} alt='logo' />
          </Link>

          <div className='w-px h-5 mt-1 ml-2' style={{ background: 'rgba(255, 255, 255, 0.08)' }} />
          <div className='flex items-center gap-[6px]'>
            <Link href='/features' className='text-[16px] font-[400] tracking-[-0.16px] px-2 text-[#FAFAFAE6]'>
              Features
            </Link>
            <Link href='/about' className='text-[16px] font-[400] tracking-[-0.16px] px-2 text-[#FAFAFAE6]'>
              About
            </Link>
          </div>
        </div>
        <div>
          {isAuthenticated ? (
            <div className='flex items-center gap-2'>
              <div className='h-[38px] flex items-center gap-2 header-info-user w-full relative'>
                <div className='flex items-center gap-2 w-full h-full rounded-[6px] px-3'>
                <Image
                src={dummyDataUser?.avatar}
                alt='logo'
                width={26}
                height={26}
                className='b+rounded-[38px] border-[#ffffff3d]'
              />
                  <span className='text-[14px] font-[400] tracking-[-0.14px] text-[#FAFAFAE6]'>
                    {dummyDataUser?.credits}
                  </span>
                  <span className='text-[14px] font-[300] tracking-[-0.28px] text-[#FAFAFAE6] opacity-50'>Credits</span>
                </div>
                <Image src={Tab_name} alt='Tab_name' className='absolute top-0 right-0 left-0' height={38} />
              </div>
              <Image
                src={dummyDataUser?.avatar}
                alt='logo'
                width={38}
                height={38}
                className='b+rounded-[38px] border-[#ffffff3d]'
              />
            </div>
          ) : (
            <div
              onClick={handleSignin}
              className='text-[16px] font-[400] tracking-[-0.16px] px-2 text-[#FAFAFAE6] hover:cursor-pointer'
            >
              Sign In
            </div>
          )}
        </div>
      </div>
      {/* <ModeToggle /> */}
      <ModalLogin open={isOpenModalLogin} onClose={handleCloseModalLogin} />
    </div>
  )
}
