import Image from 'next/image'
import React from 'react'
import { Tooltip, TooltipContent, TooltipProvider, TooltipTrigger } from '@/components/ui/tooltip'
import IconpopupContent from '../../../public/images/App/icon_popup_content.svg'
import IconpopupContentHover from '../../../public/images/App/icon_popup_content_hover.svg'
import IconpopupContentHoverArrow from '../../../public/images/App/icon_popup_content_hover_arrow.svg'
import DummyPopup from '../../../public/images/App/dummy_popup.png'
import '../style/IconPopupContent.css'
import { archivo, unbounded } from '@/app/font'

interface IconPopupContentProps {
    content: string,
    itemActive: any
}

const IconPopupContent: React.FC<IconPopupContentProps> = ({ content = '', itemActive = {} }) => {
    return (
        <TooltipProvider delayDuration={300}>
            <Tooltip>
                <TooltipTrigger asChild>
                    <div className='inline-block ml-[8px] relative w-[21px]'>
                        <div className='cursor-pointer button_show_popup'>
                            <Image src={IconpopupContent} alt="IconpopupContent" className='icon_popup' />
                            <div className='flex items-center justify-center show_popup_hover'>
                                <Image src={IconpopupContentHoverArrow} alt="IconpopupContentHoverArrow" className='icon_popup_hover_arrow' />
                                <Image src={IconpopupContentHover} alt="IconpopupContentHover" className='ml-[-4px] icon_popup_hover' width={21} height={18} />
                            </div>
                        </div>
                    </div>
                </TooltipTrigger>
                <TooltipContent side='right' sideOffset={13} className='border border-[#998dc61f] w-[524px] min-h-[300px] p-6 rounded-[8px] pt-[40px] pb-[24px] px-[24px]' style={{ background: 'linear-gradient(180deg, rgba(255, 255, 255, 0.04) 0%, rgba(255, 255, 255, 0.20) 100%)', backdropFilter: 'blur(20px)' }}>
                    <div className='flex gap-3 items-start justify-start'>
                        <Image src={itemActive?.logo} width={32} height={32} className="rounded-[32px] mt-[4px]" alt={itemActive?.user_name} />
                        <div className='flex flex-col gap-4'>
                            <div className='flex gap-1'>
                                <span className={`${unbounded.className} text-[16px] font-[400] tracking-[-0.16px] text-[#F5F5F5]`}>{itemActive?.name}</span>
                                <span className={`${archivo.className} text-[16px] font-[300] tracking-[0.32px] text-[#fafafae6] opacity-60`}>@{itemActive?.user_name}</span>
                            </div>
                            <div className='flex flex-col gap-[12px]'>
                                {content}
                                {itemActive?.content?.slice(0, 4)?.map((item: any, index: number) => (
                                    <p key={index} className={`text-[16px] font-[400] tracking-[-0.16px] text-[#F5F5F5]`}>{item}</p>
                                ))}
                                <Image src={DummyPopup} alt='DummyPopup' />
                            </div>
                        </div>
                    </div>
                </TooltipContent>
            </Tooltip>
        </TooltipProvider>
    )
}
export default IconPopupContent