import React, { useState, useEffect } from 'react';

interface CountdownTimerProps {
    initialTime: number;
    onEndCountDown?: () => void;
    type: string;
}

const CountdownTimer: React.FC<CountdownTimerProps> = ({ initialTime, onEndCountDown, type }) => {
    const [time, setTime] = useState<number>(initialTime);
    const [formattedTime, setFormattedTime] = useState<string>('');

    useEffect(() => {
        const formatTime = (seconds: number): string => {
            const hours = Math.floor(seconds / 3600);
            const minutes = Math.floor((seconds % 3600) / 60);
            const remainingSeconds = seconds % 60;
            return type === 'no_hour'
                ? `${minutes < 10 ? '0' : ''}${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`
                : `${hours < 10 ? '0' : ''}${hours}:${minutes < 10 ? '0' : ''}${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;
        };

        setFormattedTime(formatTime(time));

        if (time > 0) {
            const interval = setInterval(() => {
                setTime(prevTime => {
                    if (prevTime <= 1) {
                        clearInterval(interval);
                        if (onEndCountDown) {
                            onEndCountDown();
                        }
                        return 0;
                    }
                    return prevTime - 1;
                });
            }, 1000);

            return () => clearInterval(interval);
        }
    }, [time, onEndCountDown, type]);

    return <>{formattedTime}</>;
}

export default CountdownTimer;
