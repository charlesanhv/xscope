import React, { useEffect, useState } from 'react'
import { DetectItem } from '@/interface'
import Image from 'next/image'
import { TYPE_DETECT_ITEM } from '@/constants'
import { archivo } from '@/app/font'

interface PopupInfoProps {
  data: DetectItem
}

const PopupInfo: React.FC<PopupInfoProps> = ({ data }) => {
  const [isLoading, setIsLoading] = useState(true)

  const sizeImage = data.type === TYPE_DETECT_ITEM.type_1 ? 40 : 56

  //fake loading
  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false)
    }, 500)
  }, [])

  if (isLoading)
    return (
      <div className='fixed inset-0 flex items-center justify-center'>
        <div
          className='inline-block h-8 w-8 animate-spin duration-700 rounded-full border-4 border-solid border-current border-e-transparent align-[-0.125em] text-neutral-50 '
          role='status'
        />
      </div>
    )

  return (
    <div>
      <div className='flex gap-x-3 items-center'>
        <Image
          style={{
            borderRadius: '50%'
          }}
          src={data.image || ''}
          alt='Image'
          width={sizeImage}
          height={sizeImage}
        />
        <div>
          <div className='flex gap-x-2 items-center'>
            <span className='text-[16px] font-light text-[#F5F5F5]'>{data.name}</span>
            {data.type !== TYPE_DETECT_ITEM.type_2 && (
              <span className={`${archivo.className} text-[14px] font-normal text-[#fafafae6] opacity-60 `}>
                {data.subName}
              </span>
            )}
            <Image src={'/images/App/icon_progress.svg'} alt='Image' width={16} height={16} />
          </div>

          {data.type !== TYPE_DETECT_ITEM.type_3 ? (
            <div className={`${archivo.className} mt-2 flex gap-x-2 items-center`}>
              {data.type === TYPE_DETECT_ITEM.type_1 ? (
                <>
                  <span className='text-2xl font-semibold text-[#F5F5F5]'>{data.price}</span>
                  <span className='text-[14px] font-normal text-[#49C674]'>{data.change_24}</span>
                </>
              ) : (
                <span className='text-[14px] font-normal text-[#fafafae6] opacity-60'>{data.subName}</span>
              )}
            </div>
          ) : null}
        </div>
      </div>

      <div className={`${archivo.className} mt-5 flex items-center`}>
        <div>
          <p className='text-[#f5f5f599] text-[14px] mb-2'>
            {data.type === TYPE_DETECT_ITEM.type_2
              ? 'Related Token'
              : data.type === TYPE_DETECT_ITEM.type_3
                ? 'Total Invest'
                : 'Market Cap'}
          </p>

          {data.type === TYPE_DETECT_ITEM.type_1 && (
            <div className='flex items-center gap-x-[6px]'>
              <span className='text-[#F5F5F5] text-[14px]'>$779.52M</span>
              <div className='flex items-center gap-x-1'>
                <Image src='/images/App/arrow_up.svg' alt='Image' width={10} height={6} />
                <span className='text-[#49C674] text-xs font-semibold'>8.08%</span>
              </div>
            </div>
          )}
          {data.type === TYPE_DETECT_ITEM.type_2 && (
            <div className='flex -space-x-[8px] rtl:space-x-reverse'>
              <Image src='/images/dummy/token.svg' alt='Image' width={24} height={24} style={{ borderRadius: '50%' }} />
              <Image src='/images/dummy/token.svg' alt='Image' width={24} height={24} style={{ borderRadius: '50%' }} />
              <Image src='/images/dummy/token.svg' alt='Image' width={24} height={24} style={{ borderRadius: '50%' }} />
              <Image src='/images/dummy/token.svg' alt='Image' width={24} height={24} style={{ borderRadius: '50%' }} />
              <Image src='/images/dummy/token.svg' alt='Image' width={24} height={24} style={{ borderRadius: '50%' }} />
            </div>
          )}
          {data.type === TYPE_DETECT_ITEM.type_3 && (
            <p className='text-[#F5F5F5] text-[14px] font-semibold '>$800.52M</p>
          )}
        </div>

        {data.type !== TYPE_DETECT_ITEM.type_2 ? (
          <div className='bg-[#998dc63d] w-[1px] h-[40px] mx-4' />
        ) : (
          <div className='bg-transparent w-[1px] h-[40px] mx-4' />
        )}

        <div>
          <p className='text-[#f5f5f599] text-[14px] mb-2'>
            {data.type === TYPE_DETECT_ITEM.type_2
              ? 'Related Venture'
              : data.type === TYPE_DETECT_ITEM.type_3
                ? 'Number of Invests'
                : 'Founder'}
          </p>

          {data.type === TYPE_DETECT_ITEM.type_1 && (
            <div className='flex items-center gap-x-2'>
              <Image
                src='/images/App/logo_twitter_dummy.svg'
                alt='Image'
                width={24}
                height={24}
                style={{ borderRadius: '50%' }}
              />
              <span className='text-[#F5F5F5] text-[14px] font-semibold'>Robert Anthony</span>
            </div>
          )}
          {data.type === TYPE_DETECT_ITEM.type_2 && (
            <div className='flex -space-x-[8px] rtl:space-x-reverse'>
              <Image src='/images/dummy/token.svg' alt='Image' width={24} height={24} style={{ borderRadius: '50%' }} />
              <Image src='/images/dummy/token.svg' alt='Image' width={24} height={24} style={{ borderRadius: '50%' }} />
            </div>
          )}
          {data.type === TYPE_DETECT_ITEM.type_3 && <p className='text-[#F5F5F5] text-[14px] font-semibold'>124</p>}
        </div>
      </div>

      {data.type === TYPE_DETECT_ITEM.type_3 && (
        <div className={`${archivo.className} mt-5`}>
          <p className='text-[#f5f5f599] text-[14px]'>Founder</p>
          <div className='flex items-center gap-2 text-[#F5F5F5] text-[14px] mt-2'>
            <Image src={'/images/App/logo_twitter_dummy.svg'} alt='Image' width={24} height={24} />
            <p>Robert Anthony</p>
          </div>
        </div>
      )}

      <div className={`${archivo.className} mt-5 pr-6 line-clamp-5 text-[16px] font-normal text-[#f5f5f599]`}>
        {data.description}
      </div>

      <div className='mt-5 pt-4'>
        <div className={`${archivo.className} flex flex-wrap gap-[6px]`}>
          {(data.tags || []).map((tag, index) => (
            <div
              className='px-3 py-[2px] bg-[#c5d8ee1f] w-max rounded-[8px] text-[#F5F5F5] text-[14px] font-normal opacity-80'
              key={index}
            >
              {tag}
            </div>
          ))}
        </div>
      </div>

      <div className='mt-5 pt-3 flex'>
        <div className='p-3 pl-1'>
          <div className='flex items-center gap-x-2'>
            <Image
              src={data.type === TYPE_DETECT_ITEM.type_1 ? '/images/App/backer.svg' : '/images/App/icon_portfolio.svg'}
              alt='Image'
              width={16}
              height={16}
            />
            <span className='text-xs font-normal text-[#919DAD]'>
              {data.type === TYPE_DETECT_ITEM.type_1 ? 'Backers' : 'Portfolio'}
            </span>
          </div>

          <div className={`${archivo.className} mt-2`}>
            <span className='mr-1 text-[14px] font-normal text-[#F5F5F5]'>1000</span>
            <span className='text-[14px] font-normal text-[#acacad]'>
              {data.type === TYPE_DETECT_ITEM.type_1 ? 'investors' : 'assets'}
            </span>
          </div>
        </div>

        <div className='bg-[#998dc63d] w-[1px] h-12 mx-4' />

        <div className='p-3 pl-1'>
          <div className='flex items-center gap-x-2'>
            <Image src={'/images/App/news_item.svg'} alt='Image' width={16} height={16} />
            <span className='text-xs font-normal text-[#919DAD]'>News & Activities</span>
          </div>

          <div className={`${archivo.className} flex items-center mt-2`}>
            <div>
              <span className='mr-1 text-[14px] font-normal text-[#F5F5F5]'>252</span>
              <span className='text-[14px] font-normal text-[#acacad]'>news</span>
            </div>

            <div className='mx-[6px] text-[#36373999]'>-</div>

            <div>
              <span className='mr-1 text-[14px] font-normal text-[#F5F5F5]'>789</span>
              <span className='text-[14px] font-normal text-[#acacad]'>actitivites</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PopupInfo
