/* eslint-disable react/no-unescaped-entities */
import React from 'react'
import { Dialog, DialogContent } from '@/components/ui/dialog'
import Image from 'next/image'

import { archivo } from '@/app/font'
import Link from 'next/link'

interface ModalLoginProps {
  open: boolean
  onClose?: () => void
}

const ModalLogin: React.FC<ModalLoginProps> = ({ open, onClose }) => {
  const button = [
    {
      icon: '/images/App/icon_twitter.svg',
      name: 'Log In with Twitter',
      link: 'https://xscope.xyz/auth/twitter'
    },
    {
      icon: '/images/App/icon_warpcast.svg',
      name: 'Log In with Warpast'
    },
    {
      icon: '/images/App/icon_google.svg',
      name: 'Log In with Google',
      link: 'https://xscope.xyz/auth/google'
    }
  ]

  return (
    <Dialog open={open} onOpenChange={onClose}>
      <DialogContent className="w-[824px] h-[612px] rounded-[6px] border-none p-6 pt-16 bg-[url('/images/App/bg_login.png')] z-[600]">
        <div className='flex flex-col items-center justify-between'>
          <div className='flex flex-col items-center'>
            <div
              onClick={onClose}
              className='w-10 h-10 rounded-[50%] bg-[#000] flex items-center justify-center border-[#ffffff1f] absolute top-5 right-5 hover:cursor-pointer'
            >
              <Image src='/images/App/icon_close_2.svg' alt='logo' width={16} height={16} />
            </div>

            <div className='pt-8'>
              <Image src='/images/App/icon_x.svg' alt='logo' width={48} height={44} />
            </div>
            <p className='mt-3 mb-20 text-[#F5F5F5] text-[28px] font-medium'>Log In XScope</p>

            {button.map((item, index) => (
              <Link
                href={item.link ?? `/`}
                target='_blank'
                key={index}
                className='relative h-12 w-[480px] rounded-[4px] border-[1px] border-[#c5d8ee4d] flex items-center justify-center text-[16px] text-[#F5F5F5] font-normal mb-4 last:mb-0 hover:cursor-pointer'
              >
                <div className='absolute left-3 top-[50%] translate-y-[-50%] '>
                  <Image src={item.icon} alt='logo' width={24} height={24} />
                </div>
                {item.name}
              </Link>
            ))}
          </div>

          <span className={`${archivo.className} text-[#f5f5f599] text-[13px] font-normal`}>
            By sign in, your're agree to Statistr’s Terms of Service & Privacy Policy
          </span>
        </div>
      </DialogContent>
    </Dialog>
  )
}

export default ModalLogin
