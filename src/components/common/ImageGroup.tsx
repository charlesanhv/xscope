'use client'
import Image from 'next/image'
import '../style/App.css'
import React from 'react'
import { archivo } from '@/app/font'
import '../style/ImageGroup.css'
import Logo from '../../../public/images/App/logo_twitter_dummy.svg'
interface ImageGroupProps {
    listImage: any;
}


const ImageGroup: React.FC<ImageGroupProps> = ({ listImage }) => {
    return (
        <div>
            {listImage ? (
                <>
                    {listImage?.length > 2 ? (
                        <div className='inline-flex items-center list_images' style={{transformStyle: 'preserve-3d'}}>
                            {listImage?.slice(0,2)?.map((item: any, index: any) => (
                                <div key={index} className='border-[1.4px] rounded-full border-[#515151] w-max image-item bg-[#c4c4c4]'>
                                    <Image
                                        src={item || Logo}
                                        alt="image"
                                        width={24}
                                        height={24}
                                    />
                                </div>
                            ))}
                            <span className={`${archivo.className} text-[13px] font-[600] tracking-[-0.14px] text-[#F5F5F5] h-[24px] flex items-center justify-center bg-[#c5d8ee1f] py-1 pl-2 pr-[6px] rounded-[16px] count-text`}>+{listImage?.length - 2}</span>
                        </div>
                    ) : (
                        <div>
                            {listImage?.map((item: any, index: any) => (
                                <div key={index}>
                                    <Image
                                        src={item || Logo}
                                        alt="image"
                                        width={24}
                                        height={24}
                                    />
                                </div>
                            ))}
                        </div>
                    )}
                </>
            ) : null}
        </div>
    );
}

export default ImageGroup
