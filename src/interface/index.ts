export interface DetectItem {
  image?: string
  name?: string
  subName?: string
  type?: string
  price?: string
  change_24?: string
  description?: string
  tags?: string[]
  number_of_invests?: number
  total_invests?: string
}

export interface DetectedItemProps {
  image: string
  name: string
  subName: string
}
